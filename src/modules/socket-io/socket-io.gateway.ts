import {
    ConnectedSocket,
    MessageBody,
    OnGatewayConnection,
    OnGatewayDisconnect,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { Body, Delete, Get, Post, UseFilters, UseGuards, UsePipes } from '@nestjs/common';
import { Envs } from '../../common/envs/env';
import { logger } from '../../common/logger/logger';
import { ApiController } from '../../common/decorators/swagger/api-controller.decorator';
import { CustomSubscribeMessage } from '../../common/decorators/metadata/custom-subscrube-message/custom-subscribe-message.decorator';
import { ChatsService } from '../chats/chats.service';
import { UserId } from '../../common/decorators/requests/user-id.decorator';
import { QueryGetChatsDto } from '../chats/dto/requests/query-get-chats.dto';
import { DataResponse } from '../../common/dto/responses/data-response';
import { ChatListDto } from '../chats/dto/responses/chat-list/chat-list.dto';
import { ApiSendResponse } from '../../common/decorators/swagger/api-send-response.decorator';
import { Public } from '../../common/decorators/metadata/public/public.decorator';
import { AuthService } from '../auth/services/auth.service';
import { PayloadDto } from '../auth/dto/responses/payload.dto';
import { CheckProofDto } from '../auth/dto/requests/check-proof/check-proof.dto';
import { TokenDto } from '../auth/dto/responses/token.dto';
import { AuthDto } from '../auth/dto/responses/auth.dto';
import { RegistrationDto } from '../auth/dto/requests/registration.dto';
import { LoginDto } from '../auth/dto/requests/login.dto';
import { AuthTokenDto } from '../auth/dto/responses/auth-token.dto';
import { ApiNoContentResponse } from '../../common/decorators/swagger/api-no-content-response.decorator';
import { ConnectDto } from '../auth/dto/requests/connect.dto';
import { AuthConnectDto } from '../auth/dto/responses/auth-connect.dto';
import { DeleteSessionsDto } from '../auth/dto/requests/delete-sessions.dto';
import { SessionsDto } from '../auth/dto/responses/sessions.dto';
import { CreateChatDto } from '../chats/dto/requests/create-chat.dto';
import { ChatDto } from '../chats/dto/responses/chat-list/chat.dto';
import { UsersService } from '../users/users.service';
import { GetUsersDto } from '../users/dto/requests/get-users.dto';
import { UsersListDto } from '../users/dto/responses/users-list.dto';
import { RoomJoinDto } from '../auth/dto/requests/room-join.dto';
import { Request } from '../../common/decorators/requests/request.decorator';
import { CustomRequestType } from '../../common/types/custom-request.type';
import { CreateDialogDto } from '../chats/dto/responses/create-dialog.dto';
import { CreateMessageDto } from '../chats/dto/responses/create-message.dto';
import { QueryGetMessagesDto } from '../chats/dto/requests/query-get-messages.dto';
import { MessageDto } from '../chats/dto/responses/message.dto';
import { ParseJsonPipe } from './middlewears/parse-json.pipe';
import { WebsocketExceptionsFilter } from './middlewears/exception.filter';
import { EmitGuard } from './middlewears/emit.guard';
import { AuthGuard } from './middlewears/auth.guard';
import { socketServer } from './raw/socket-server';

@ApiController()
@UseGuards(EmitGuard, AuthGuard)
@UseFilters(WebsocketExceptionsFilter)
@UsePipes(ParseJsonPipe)
@WebSocketGateway(Envs.main.socketIoPort, {
    cors: true,
    methods: ['GET', 'POST'],
    path: '/chats',
})
export class SocketIoGateway implements OnGatewayConnection, OnGatewayDisconnect {
    @WebSocketServer() server!: Server;

    constructor(
        private readonly chatsService: ChatsService,
        private readonly authService: AuthService,
        private readonly usersService: UsersService,
    ) {}

    onModuleInit(): void {
        if (!socketServer && this.server) {
            // @ts-ignore
            socketServer = this.server;
        }
    }

    handleConnection(@ConnectedSocket() client: Socket): Promise<void> | void {
        logger.info(`Client connected: ${client.id}`);
    }

    handleDisconnect(@ConnectedSocket() client: Socket): Promise<void> {
        return this.authService.deleteUserConnection(client.id);
    }

    @ApiSendResponse(ChatListDto, 'получения чатов', true)
    @CustomSubscribeMessage('chats:get', Get)
    getChats(@UserId() userId: string, @MessageBody() body: QueryGetChatsDto): Promise<DataResponse<ChatListDto>> {
        return this.chatsService.getChats(userId, body.id, body.key, body.type, body.limit, body.offset);
    }

    @ApiSendResponse(ChatDto, 'создания чата')
    @CustomSubscribeMessage('chats:create', Post)
    createChat(@MessageBody() body: CreateChatDto, @UserId() userId: string): Promise<DataResponse<ChatDto>> {
        return this.chatsService.createChat(userId, body.key, body.isPublic, body.encryptAesKey, body.title);
    }

    @ApiSendResponse(MessageDto, 'получения сообщений', true)
    @CustomSubscribeMessage('chats:messages:get', Get)
    getMessages(
        @MessageBody() query: QueryGetMessagesDto,
        @UserId() userId: string,
    ): Promise<DataResponse<MessageDto[]>> {
        return this.chatsService.getMessages(query.id, userId, query.limit, query.offset);
    }

    @CustomSubscribeMessage('dialog:send', Post)
    createDialog(@MessageBody() body: CreateDialogDto, @UserId() userId: string) {
        return this.chatsService.dialogSend(
            userId,
            body.encryptMessage,
            body.address,
            body.senderEncryptAesKey,
            body.recipientEncryptAesKey,
        );
    }

    @CustomSubscribeMessage('chats:message:create', Post)
    createChatMessage(@UserId() userId: string, @MessageBody() body: CreateMessageDto): Promise<DataResponse<object>> {
        return this.chatsService.createMessage(userId, body.chatId, body.encryptMessage);
    }

    @Public()
    @CustomSubscribeMessage('server:state:get', Get)
    getError() {
        return {
            data: {},
            success: true,
        };
    }

    @Public()
    @ApiSendResponse(PayloadDto, 'получения ключа для входа в ton кошелек')
    @CustomSubscribeMessage('auth:payload:get', Get)
    getPayload(): DataResponse<PayloadDto> {
        return this.authService.getPayload();
    }

    @Public()
    @ApiSendResponse(TokenDto, 'получения токена авторизации')
    @CustomSubscribeMessage('auth:proof', Post)
    checkProof(@MessageBody() body: CheckProofDto): Promise<DataResponse<TokenDto>> {
        return this.authService.checkProof(body);
    }

    @Public()
    @ApiSendResponse(AuthTokenDto, 'регистрации')
    @CustomSubscribeMessage('auth:registration', Post)
    registration(
        @ConnectedSocket() client: Socket,
        @Body() body: RegistrationDto,
    ): Promise<DataResponse<AuthTokenDto>> {
        return this.authService.registration(
            client.id,
            body.payloadToken,
            body.hashPassphrase,
            body.hashUserAgent,
            body.publicKey,
            body.encryptPrivateKey,
        );
    }

    @Public()
    @ApiSendResponse(AuthDto, 'входа')
    @CustomSubscribeMessage('auth:login', Post)
    login(@ConnectedSocket() client: Socket, @Body() body: LoginDto): Promise<DataResponse<AuthDto>> {
        return this.authService.login(client.id, body.payloadToken, body.hashPassphrase, body.hashUserAgent);
    }

    @ApiNoContentResponse('выхода из аккаунта')
    @CustomSubscribeMessage('auth:logout', Delete)
    logout(@ConnectedSocket() client: Socket, @UserId() userId: string): Promise<DataResponse<object>> {
        return this.authService.logout(client.id, userId);
    }

    @Public()
    @ApiSendResponse(AuthConnectDto, 'активации сессии')
    @CustomSubscribeMessage('auth:connect', Post)
    connect(
        @ConnectedSocket() client: Socket,
        @Body() body: ConnectDto,
        @Request() request: CustomRequestType,
    ): Promise<DataResponse<AuthConnectDto>> {
        return this.authService.addUserConnection(
            request,
            client.id,
            body.authToken,
            body.hashPassphrase,
            body.hashUserAgent,
        );
    }

    @ApiSendResponse(SessionsDto, 'удаления сессии')
    @CustomSubscribeMessage('sessions:delete', Delete)
    deleteSession(
        @ConnectedSocket() client: Socket,
        @UserId() userId: string,
        @MessageBody() body: DeleteSessionsDto,
    ): Promise<DataResponse<SessionsDto>> {
        return this.authService.deleteSessions(client.id, userId, body.encryptUserAgents);
    }

    @ApiSendResponse(UsersListDto, 'получения пользователей', true)
    @CustomSubscribeMessage('users:get', Get)
    getUsers(@MessageBody() query: GetUsersDto): Promise<DataResponse<UsersListDto>> {
        return this.usersService.getUser(query.address, query.limit, query.offset);
    }

    @ApiNoContentResponse('подключения к комнате')
    @CustomSubscribeMessage('rooms:join', Post)
    joinRoom(@ConnectedSocket() client: Socket, @MessageBody() body: RoomJoinDto): void {
        return this.authService.joinRoom(client, body.room, body.id);
    }

    @ApiNoContentResponse('выхода из комнате')
    @CustomSubscribeMessage('rooms:leave', Delete)
    leaveRoom(@ConnectedSocket() client: Socket, @MessageBody() body: RoomJoinDto): void {
        return this.authService.leaveRoom(client, body.room, body.id);
    }
}
