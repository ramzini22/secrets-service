import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { CustomRequestType } from '../../../common/types/custom-request.type';
import { EVENT_NAME_METADATA_KEY } from '../../../common/decorators/metadata/custom-subscrube-message/event-name.metadata-key';

@Injectable()
export class EmitGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean {
        const request = context.switchToHttp().getRequest<CustomRequestType>();
        const eventName = this.reflector.get<string>(EVENT_NAME_METADATA_KEY, context.getHandler());

        request.eventName = String(eventName);

        return true;
    }
}
