import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { createCustomBadRequestError } from '../../../common/utils/error.utils';

@Injectable()
export class ParseJsonPipe implements PipeTransform {
    async transform(value: object, metadata: ArgumentMetadata) {
        if (metadata.type === 'body') {
            const metatype = metadata.metatype;
            let object;
            let errors;

            if (!metatype || !this.toValidate(metatype)) {
                return value;
            }

            try {
                object = <object>plainToInstance(metatype, JSON.parse(JSON.stringify(value)));
                errors = await validate(object, {
                    whitelist: true,
                    forbidNonWhitelisted: true,
                });
            } catch (e) {
                throw createCustomBadRequestError('Не удалось распарсить тело запроса.');
            }

            if (errors && errors.length > 0) throw createCustomBadRequestError(errors);

            return object;
        } else {
            return value;
        }
    }
    private toValidate(metatype: unknown): boolean {
        const types: unknown[] = [String, Boolean, Number, Array, Object];

        return !types.includes(metatype);
    }
}
