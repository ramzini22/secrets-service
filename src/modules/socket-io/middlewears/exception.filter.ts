import { ArgumentsHost, Catch, HttpException, ValidationError } from '@nestjs/common';
import { BaseWsExceptionFilter, WsException } from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { CustomError, errorEnum } from '../../../common/errors/custom-error';
import { CustomRequestType } from '../../../common/types/custom-request.type';
import { detailType } from '../../../common/types/detail.type';
import { DataError, validationError } from '../../../common/dto/responses/data-error.dto';

@Catch(WsException, HttpException)
export class WebsocketExceptionsFilter extends BaseWsExceptionFilter {
    catch(exception: CustomError, host: ArgumentsHost): void {
        const client = host.switchToWs().getClient<Socket>();
        const request = host.switchToHttp().getRequest<CustomRequestType>();
        const callback = host.getArgByIndex<object>(2);

        const eventName = request.eventName;

        let dataError: DataError;

        if (eventName) {
            const error = exception.getError();

            const details: (validationError | string)[] = [];

            if (typeof error === 'string') details.push(error);
            else
                exception.details.map((detail) => {
                    if (detail.constraints)
                        Object.keys(detail.constraints).map((key) =>
                            details.push({
                                error: detail.constraints ? detail.constraints[key] : '',
                                column: detail.property,
                            }),
                        );
                });

            dataError = new DataError(exception.type, details, eventName);
        } else dataError = new DataError(errorEnum.SERVER_ERROR, ['Неизвестаня ошибка сервера.'], eventName);

        if (callback && typeof callback === 'function') callback(dataError);

        client.emit('errors', dataError);
    }

    private getDetails = (details: ValidationError[] | string): detailType[] => {
        if (typeof details == 'string') return [{ error: details }];

        const allDetails: detailType[] = [];

        details.map((detail) => {
            if (detail.constraints)
                Object.keys(detail.constraints).map((key) =>
                    allDetails.push({ error: detail.constraints ? detail.constraints[key] : key }),
                );

            if (detail.children?.length) allDetails.push(...this.getDetails(detail.children));
        });

        return allDetails;
    };
}
