import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Socket } from 'socket.io';
import { CustomRequestType } from '../../../common/types/custom-request.type';
import { IS_PUBLIC_METADATA_KEY } from '../../../common/decorators/metadata/public/cerification-type.metadata-key';
import { UsersConnectionsService } from '../raw/users-connections.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private reflector: Reflector,
        private readonly usersConnectionsService: UsersConnectionsService,
    ) {}

    canActivate(context: ExecutionContext): boolean {
        const isPublic = this.getVerificationType(context);
        const client = context.switchToWs().getClient<Socket>();
        const request = context.switchToHttp().getRequest<CustomRequestType>();
        const userAgent = request.handshake.headers['user-agent'];

        request.userAgent = String(userAgent);
        request.context = context;

        if (isPublic) return true;

        const user = this.usersConnectionsService.getUserBySocketId(client.id);

        if (!user) return false;

        request.userId = user.id;
        request.address = user.address;

        return true;
    }

    private getVerificationType(context: ExecutionContext): boolean {
        return (
            this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_METADATA_KEY, [
                context.getHandler(),
                context.getClass(),
            ]) ?? false
        );
    }
}
