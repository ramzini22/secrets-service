import { Injectable } from '@nestjs/common';
import md5 from 'md5';
import { User } from '../../users/entities/user.entity';
import { UserSessionsRedis } from '../../caches/user-sessions/user-sessions.redis';
import { SessionType } from '../../caches/sessions/session.type';
import { AuthConnectDto } from '../../auth/dto/responses/auth-connect.dto';
import { SessionsDto } from '../../auth/dto/responses/sessions.dto';
import { DataResponse } from '../../../common/dto/responses/data-response';
import { SessionDto } from '../../auth/dto/responses/session.dto';

@Injectable()
export class UsersConnectionsService {
    // userId->socketId[]
    private readonly userConnections: Map<string, Set<string>>;
    // socketId->sessionType
    private readonly connectionSessions: Map<string, SessionType>;

    constructor(private readonly userSessionsRedis: UserSessionsRedis) {
        this.userConnections = new Map<string, Set<string>>();
        this.connectionSessions = new Map<string, SessionType>();
    }

    public getUserBySocketId(socketId: string): User | null {
        const session = this.connectionSessions.get(socketId);

        return session?.user ?? null;
    }

    public async getSession(authToken: string): Promise<SessionType | null> {
        const hashAuthToken = md5(authToken);

        return this.userSessionsRedis.getSession(hashAuthToken);
    }

    public async addSession(socketId: string, hashUserAgent: string, authToken: string, user: User): Promise<void> {
        const hashAuthToken = md5(authToken);
        const userConnections = this.userConnections.get(user.id) ?? new Set<string>();
        this.userConnections.set(user.id, userConnections.add(socketId));

        const session = await this.userSessionsRedis.addSession(
            hashAuthToken,
            new User({ ...user, keyPair: undefined }),
            hashUserAgent,
        );
        this.connectionSessions.set(socketId, session);
    }

    public async deleteSession(socketId: string): Promise<void> {
        const session = this.connectionSessions.get(socketId);

        if (!session) return;

        this.connectionSessions.delete(socketId);
        await this.userSessionsRedis.deleteSession(session.hashAuthToken);

        const userConnections = this.userConnections.get(session.user.id);

        if (!userConnections) return;

        userConnections.delete(socketId);

        if (userConnections?.size) this.userConnections.set(session.user.id, userConnections);
        else userConnections.delete(session.user.id);
    }

    public async deleteSessions(userId: string, encryptUserAgents: string[]): Promise<DataResponse<SessionsDto>> {
        const userSessionsRedis = await this.userSessionsRedis.get(userId);

        if (!userSessionsRedis) return new DataResponse<SessionsDto>();

        const existUserSessions = userSessionsRedis?.filter(
            ({ encryptUserAgent }) => !encryptUserAgents.includes(encryptUserAgent),
        );

        const deleteUserSessions = userSessionsRedis.filter((session) =>
            encryptUserAgents.includes(session.encryptUserAgent),
        );

        await Promise.all(
            deleteUserSessions.map((session) => this.userSessionsRedis.deleteSession(session.hashAuthToken)),
        );

        if (existUserSessions?.length) await this.userSessionsRedis.set(userId, existUserSessions);
        else await this.userSessionsRedis.delete(userId);

        const userSocketIds = Array.from(this.userConnections.get(userId) ?? []);

        const socketIds = userSocketIds.filter((socketId) => {
            const session = this.connectionSessions.get(socketId);

            if (!session) return false;

            if (deleteUserSessions.find((deleteSession) => deleteSession.hashAuthToken === session.hashAuthToken)) {
                this.connectionSessions.delete(socketId);

                return false;
            }

            return true;
        });

        this.userConnections.set(userId, new Set(socketIds));

        return this.getSessionsByUserId(userId);
    }

    public async reconnectSession(
        socketId: string,
        hashUserAgent: string,
        authToken: string,
        user: User,
        authTokenNew = authToken,
    ): Promise<AuthConnectDto> {
        const userConnections = this.userConnections.get(user.id) ?? new Set<string>();
        this.userConnections.set(user.id, userConnections.add(socketId));

        const hashAuthTokenNew = authTokenNew ? md5(authTokenNew) : authToken;

        const session = await this.userSessionsRedis.updateSession(
            hashAuthTokenNew,
            hashAuthTokenNew,
            user,
            hashUserAgent,
        );

        this.connectionSessions.set(socketId, session);

        return new AuthConnectDto(authTokenNew ?? authToken);
    }

    public async getSessionsBySocketId(socketId: string): Promise<DataResponse<SessionsDto>> {
        const session = this.connectionSessions.get(socketId);

        if (!session) return new DataResponse<SessionsDto>();

        const user = session.user;

        return this.getSessionsByUserId(user.id);
    }

    public async getSessionsByUserId(userId: string): Promise<DataResponse<SessionsDto>> {
        const userConnections = this.userConnections.get(userId) ?? new Set<string>();

        const redisSessions = await this.userSessionsRedis.get(userId);

        const sessions = redisSessions
            ? redisSessions.map(
                  (session) =>
                      new SessionDto(
                          session.encryptUserAgent,
                          !!Array.from(userConnections).find(
                              (socketId) =>
                                  this.connectionSessions.get(socketId)?.hashAuthToken === session.hashAuthToken,
                          ),
                          session.updatedAt,
                      ),
              )
            : [];

        return new DataResponse(new SessionsDto(sessions));
    }

    public disconnectUser(socketId: string): void {
        const session = this.connectionSessions.get(socketId);

        if (!session) return;

        const userId: string = session.user.id;

        this.connectionSessions.delete(socketId);
        const userConnections = this.userConnections.get(userId);

        if (!userConnections) return;

        userConnections.delete(socketId);

        if (userConnections?.size) this.userConnections.set(userId, userConnections);
        else this.userConnections.delete(userId);
    }

    public getUserConnectionsByUserId(userId: string): string[] {
        const connections = this.userConnections.get(userId);

        return connections ? Array.from(connections) : [];
    }

    public getUserConnectionsBySocketId(socketId: string): string[] {
        const session = this.connectionSessions.get(socketId);

        if (!session) return [];

        return this.getUserConnectionsByUserId(session.user.id);
    }
}
