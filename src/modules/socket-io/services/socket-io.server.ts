import { Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';
import { UsersConnectionsService } from '../raw/users-connections.service';
import { socketServer } from '../raw/socket-server';

@Injectable()
export class SocketIoServer {
    constructor(private readonly usersConnectionsService: UsersConnectionsService) {}

    public emitAllUserDevicesByUserId(userId: string, eventName: string, data: unknown): void {
        const socketIds = this.usersConnectionsService.getUserConnectionsByUserId(userId);

        if (socketIds.length == 0) return;

        socketServer.to(socketIds).emit(eventName, data);
    }

    public emitAllUserDevicesBySocketId(socketId: string, eventName: string, data: unknown): void {
        const socketIds = this.usersConnectionsService.getUserConnectionsBySocketId(socketId);
        socketServer.to(socketIds).emit(eventName, data);
    }

    public emit(room: string | string[], eventName: string, data: unknown): void {
        socketServer.to(room).emit(eventName, data);
    }

    public emitRoom(room: string, data: unknown) {
        socketServer.to(room).emit(`${room}:listen`, data);
    }

    public joinRoom(client: Socket, roomName: string) {
        client.join(roomName);
    }

    public leaveRoom(client: Socket, roomName: string) {
        client.leave(roomName);
    }
}
