import { Module } from '@nestjs/common';
import { Envs } from '../../common/envs/env';
import { ChatsModule } from '../chats/chats.module';
import { AuthModule } from '../auth/auth.module';
import { CachesModule } from '../caches/caches.module';
import { UsersModule } from '../users/users.module';
import { SocketIoGateway } from './socket-io.gateway';
import { SocketIoServer } from './services/socket-io.server';

@Module({
    imports: [ChatsModule, AuthModule, CachesModule, UsersModule],
    controllers: Envs.swagger.isWriteConfig ? [SocketIoGateway] : [],
    providers: [SocketIoGateway, SocketIoServer],
    exports: [SocketIoServer],
})
export class SocketIoModule {}
