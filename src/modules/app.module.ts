import { Module } from '@nestjs/common';
import { getMikroOrmModule } from '../common/mikro-orm/config/mikro-orm.module';
import { MigrationService } from '../common/mikro-orm/config/migration.service';
import { SocketIoModule } from './socket-io/socket-io.module';
import { UsersModule } from './users/users.module';

@Module({
    imports: [getMikroOrmModule(), SocketIoModule, UsersModule],
    providers: [MigrationService],
})
export class AppModule {}
