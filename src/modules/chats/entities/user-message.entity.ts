import { Entity, Enum, Index, ManyToOne, Property, Unique } from '@mikro-orm/core';
import { User } from '../../users/entities/user.entity';
import { MessageStatusEnum } from '../types/message-status.enum';
import { CreatedUpdatedEntity } from '../../../common/mikro-orm/entities/created-updated.entity';
import { UserMessagesRepository } from '../repositories/user-messages.repository';
import { Message } from './message.entity';
import { Chat } from './chat.entity';

@Entity({ tableName: 'user_messages', repository: () => UserMessagesRepository })
@Index({ properties: ['chatId', 'userId'] })
@Index({ properties: ['status'] })
@Index({ properties: ['createdAt'] })
@Unique({ properties: ['messageId', 'userId'] })
export class UserMessage extends CreatedUpdatedEntity {
    @Property({ persist: false })
    readonly chatId!: string;

    @Property({ persist: false })
    readonly messageId!: string;

    @Property({ persist: false })
    readonly userId!: string;

    @Enum({ nativeEnumName: 'user_message_enum', items: () => MessageStatusEnum, default: MessageStatusEnum.IS_SENT })
    readonly status: MessageStatusEnum;

    constructor(chat: Chat, message: Message, user: User, status = MessageStatusEnum.IS_SENT) {
        super();
        this.chat = chat;
        this.message = message;
        this.user = user;
        this.status = status;
        this.userId = user.id;
    }

    @ManyToOne(() => Chat)
    readonly chat: Chat;

    @ManyToOne(() => Message)
    readonly message: Message;

    @ManyToOne(() => User)
    readonly user: User;
}
