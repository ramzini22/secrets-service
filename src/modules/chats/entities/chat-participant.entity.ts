import { Entity, Index, ManyToOne, Property, Unique } from '@mikro-orm/core';
import { CreatedUpdatedDeletedEntity } from '../../../common/mikro-orm/entities/created-updated-deleted.entity';
import { User } from '../../users/entities/user.entity';
import { ChatParticipantsRepository } from '../repositories/chat-participants.repository';
import { Chat } from './chat.entity';

@Entity({ tableName: 'chat_participants', repository: () => ChatParticipantsRepository })
@Index({ properties: ['userId'] })
@Unique({ properties: ['chatId', 'userId'] })
export class ChatParticipant extends CreatedUpdatedDeletedEntity {
    @Property({ persist: false })
    readonly chatId!: string;

    @Property({ persist: false })
    readonly userId!: string;

    @Property({ nullable: true, columnType: 'varchar(4096)' })
    readonly encryptAesKey: string;

    @Property({ default: 0 })
    readonly countIsSendMessages!: number;

    constructor(chat: Chat, user: User, encryptAesKey: string) {
        super();
        this.chat = chat;
        this.user = user;
        this.encryptAesKey = encryptAesKey;
    }

    @ManyToOne(() => Chat)
    readonly chat: Chat;

    @ManyToOne(() => User)
    readonly user: User;
}
