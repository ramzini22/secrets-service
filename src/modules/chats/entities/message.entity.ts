import { Collection, Entity, Index, OneToMany, Property } from '@mikro-orm/core';
import { CreatedUpdatedEntity } from '../../../common/mikro-orm/entities/created-updated.entity';
import { MessagesRepository } from '../repositories/messages.repository';
import { UserMessage } from './user-message.entity';

@Entity({ tableName: 'messages', repository: () => MessagesRepository })
@Index({ properties: ['createdAt'] })
export class Message extends CreatedUpdatedEntity {
    @Property({ columnType: 'varchar(32768)' })
    readonly encryptMessage: string;

    constructor(encryptMessage: string) {
        super();
        this.encryptMessage = encryptMessage;
    }

    @OneToMany(() => UserMessage, (userMessage) => userMessage.message)
    readonly userMessages = new Collection<UserMessage>(this);
}
