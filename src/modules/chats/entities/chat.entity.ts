import { Collection, Entity, Enum, ManyToOne, OneToMany, OneToOne, Property } from '@mikro-orm/core';
import { CreatedUpdatedEntity } from '../../../common/mikro-orm/entities/created-updated.entity';
import { ChatsRepository } from '../repositories/chats.repository';
import { User } from '../../users/entities/user.entity';
import { ChatTypesEnum } from '../types/chat-types.enum';
import { UserMessage } from './user-message.entity';
import { ChatParticipant } from './chat-participant.entity';
import { Message } from './message.entity';

@Entity({ tableName: 'chats', repository: () => ChatsRepository })
export class Chat extends CreatedUpdatedEntity {
    @Property({ nullable: true })
    readonly key!: string;

    @Property({ default: true })
    readonly isPublic: boolean;

    @Property({ nullable: true })
    readonly avatar!: string;

    @Property({ nullable: true, name: 'title' })
    readonly title!: string;

    @Property({ persist: false })
    readonly lastMessageId!: string;

    @Property({ persist: false })
    readonly creatorUserId!: string;

    @Enum({ nativeEnumName: 'chat_statuses_enum', items: () => ChatTypesEnum })
    readonly type: ChatTypesEnum;

    constructor(
        isPublic: boolean,
        creatorUser: User,
        type: ChatTypesEnum,
        key?: string,
        title?: string,
        avatar?: string,
    ) {
        super();
        this.isPublic = isPublic;
        this.creatorUser = creatorUser;
        this.type = type;

        if (key) this.key = key;

        if (title) this.title = title;

        if (avatar) this.avatar = avatar;
    }

    @OneToMany(() => UserMessage, (userMessage) => userMessage.chat)
    readonly userMessages = new Collection<UserMessage>(this);

    @OneToMany(() => ChatParticipant, (chatParticipant) => chatParticipant.chat)
    readonly chatParticipants = new Collection<ChatParticipant>(this);

    @OneToMany(() => ChatParticipant, (c) => c.chat)
    readonly actualChatParticipant = new Collection<ChatParticipant>(this);

    @Property({ persist: false })
    readonly equal_key!: number;

    @Property({ persist: false })
    readonly order_created!: Date;

    @OneToOne(() => Message, { nullable: true })
    readonly lastMessage!: Message;

    @Property({ persist: false })
    readonly count_participants!: string;

    @ManyToOne(() => User)
    readonly creatorUser: User;
}
