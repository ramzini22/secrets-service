import { Injectable } from '@nestjs/common';
import { DataResponse } from '../../common/dto/responses/data-response';
import { SocketIoServer } from '../socket-io/services/socket-io.server';
import { UsersRepository } from '../users/repositories/users.repository';
import { UsersExceptions } from '../users/exceptions/users.exceptions';
import { ChatsRepository } from './repositories/chats.repository';
import { ChatListDto } from './dto/responses/chat-list/chat-list.dto';
import { ChatDto } from './dto/responses/chat-list/chat.dto';
import { ChatParticipantsRepository } from './repositories/chat-participants.repository';
import { ChatsExceptions } from './exceptions/chats.exceptions';
import { UserMessagesRepository } from './repositories/user-messages.repository';
import { MessagesRepository } from './repositories/messages.repository';
import { ChatTypesEnum } from './types/chat-types.enum';
import { MessageDto } from './dto/responses/message.dto';

@Injectable()
export class ChatsService {
    constructor(
        private readonly socketIoServer: SocketIoServer,
        private readonly usersRepository: UsersRepository,
        private readonly chatsRepository: ChatsRepository,
        private readonly chatParticipantsRepository: ChatParticipantsRepository,
        private readonly userMessagesRepository: UserMessagesRepository,
        private readonly messagesRepository: MessagesRepository,
    ) {}

    public async dialogSend(
        userId: string,
        encryptMessage: string,
        recipientAddress: string,
        senderEncryptAesKey: string,
        recipientEncryptAesKey: string,
    ) {
        const recipientUser = await this.usersRepository.findOne({ address: recipientAddress });

        if (!recipientUser) throw UsersExceptions.isNotFoundUser(recipientAddress);

        const dialog = await this.chatsRepository.getDialogByUserIds(userId, recipientUser.id);

        if (dialog) return this.createMessage(userId, dialog.id, encryptMessage);

        const isPublic = false;

        const chatEntity = await this.chatsRepository.createEntity(
            isPublic,
            userId,
            senderEncryptAesKey,
            ChatTypesEnum.DIALOG,
        );

        if (recipientUser.id !== userId)
            await this.chatParticipantsRepository.createChatParticipant(
                chatEntity.id,
                recipientUser.id,
                recipientEncryptAesKey,
            );

        return this.createMessage(userId, chatEntity.id, encryptMessage);
    }

    public async createMessage(userId: string, chatId: string, encryptMessage: string): Promise<DataResponse<object>> {
        const chatParticipant = await this.chatParticipantsRepository.findOne({ chatId, userId });

        if (!chatParticipant) throw ChatsExceptions.isNotParticipant(chatId);

        const message = await this.messagesRepository.createMessage(encryptMessage);

        await Promise.all([
            this.userMessagesRepository.createUserMessages(chatId, message.id, userId),
            this.chatsRepository.updateOne({ id: chatId }, { lastMessageId: message.id }),
        ]);

        const chatEntity = await this.chatsRepository.getChatByIdWithParticipants(chatId);

        if (!chatEntity) throw ChatsExceptions.notFound(chatId);

        chatEntity.chatParticipants.getItems().forEach((chatPar) => {
            const response = new DataResponse(ChatDto.getFromChatParticipant(chatPar));

            this.socketIoServer.emitAllUserDevicesByUserId(chatPar.userId, 'chats:listen', response);
        });

        return new DataResponse().setSuccess(true);
    }

    public async getMessages(
        chatId: string,
        userId: string,
        limit?: number,
        offset?: number,
    ): Promise<DataResponse<MessageDto[]>> {
        const response = new DataResponse<MessageDto[]>();

        const messages = await this.userMessagesRepository.getMessages(chatId, userId, limit, offset);
        response.setData(MessageDto.getFromUserMessageEntities(messages));

        return response;
    }

    public async createChat(
        userId: string,
        key: string,
        isPublic: boolean,
        encryptAesKey: string,
        title?: string,
        avatar?: string,
    ): Promise<DataResponse<ChatDto>> {
        const response = new DataResponse<ChatDto>();

        const chatEntity = await this.chatsRepository.createEntity(
            isPublic,
            userId,
            encryptAesKey,
            ChatTypesEnum.CHAT,
            key,
            title,
            avatar,
        );
        response.setData(ChatDto.getFromChatEntity(chatEntity));
        this.socketIoServer.emitAllUserDevicesByUserId(userId, 'chats:listen', response);

        return response;
    }

    public getChats(
        userId: string,
        id?: string,
        key?: string,
        type?: ChatTypesEnum,
        limit?: number,
        offset?: number,
    ): Promise<DataResponse<ChatListDto>> {
        return this.getUserChats(userId, id, key, type, limit, offset);
    }

    private async getUserChats(
        userId: string,
        id?: string,
        key?: string,
        type?: ChatTypesEnum,
        limit?: number,
        offset?: number,
    ) {
        const response = new DataResponse<ChatListDto>();

        const [chats, count] = await Promise.all([
            this.chatsRepository.getUserChats(userId, id, key, type, limit, offset),
            this.chatParticipantsRepository.getCountUserChats(userId, key),
        ]);

        return response.setData(ChatListDto.getFromChats(chats, count));
    }
}
