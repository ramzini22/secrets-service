import { CustomBaseRepository } from '../../../common/mikro-orm/repositories/custom-base.repository';
import { Message } from '../entities/message.entity';

export class MessagesRepository extends CustomBaseRepository<Message> {
    public async createMessage(encryptMessage: string): Promise<Message> {
        const messageEntity = new Message(encryptMessage);
        await this.insert(messageEntity);

        return messageEntity;
    }
}
