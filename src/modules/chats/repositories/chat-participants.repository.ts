import { CustomBaseRepository } from '../../../common/mikro-orm/repositories/custom-base.repository';
import { ChatParticipant } from '../entities/chat-participant.entity';
import { User } from '../../users/entities/user.entity';
import { Chat } from '../entities/chat.entity';

export class ChatParticipantsRepository extends CustomBaseRepository<ChatParticipant> {
    protected ALIAS = 'chatParticipant';
    public async createChatParticipant(
        chatId: string,
        userId: string,
        encryptAesKey: string,
    ): Promise<ChatParticipant> {
        const chatParticipant = new ChatParticipant(
            this.em.getReference<Chat>(Chat, chatId),
            this.em.getReference(User, userId),
            encryptAesKey,
        );

        await this.insert(chatParticipant);

        return chatParticipant;
    }

    public getCountUserChats(userId: string, key?: string): Promise<number> {
        const qb = this.getSelectQueryBuilder().where({ userId });

        if (key)
            qb.innerJoin('chatParticipant.chat', 'chat', {
                $or: [{ 'chat.key': { $ilike: `%${key}%` } }, { 'chat.title': { $ilike: `%${key}%` } }],
            });

        return qb.getCount();
    }
}
