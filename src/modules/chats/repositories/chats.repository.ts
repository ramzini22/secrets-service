import { raw, SelectQueryBuilder } from '@mikro-orm/postgresql';
import { Chat } from '../entities/chat.entity';
import { CustomBaseRepository } from '../../../common/mikro-orm/repositories/custom-base.repository';
import { ChatParticipant } from '../entities/chat-participant.entity';
import { User } from '../../users/entities/user.entity';
import { ChatTypesEnum } from '../types/chat-types.enum';

export class ChatsRepository extends CustomBaseRepository<Chat> {
    protected ALIAS = 'chat';
    public async createEntity(
        isPublic: boolean,
        creatorUserId: string,
        encryptAesKey: string,
        type: ChatTypesEnum,
        key?: string,
        title?: string,
        avatar?: string,
    ): Promise<Chat> {
        const user = this.em.getReference<User>(User, creatorUserId);
        const chat = new Chat(isPublic, user, type, key, title, avatar);
        await this.insert(chat);

        const chatParticipant = new ChatParticipant(chat, user, encryptAesKey);
        await this.em.insert<ChatParticipant>(ChatParticipant, chatParticipant);

        chat.chatParticipants.add(chatParticipant);

        return chat;
    }

    private getChats(key?: string, limit?: number, offset?: number): SelectQueryBuilder<Chat> {
        const qb = this.getSelectQueryBuilder()
            .select('*')
            .leftJoinAndSelect('chat.lastMessage', 'last_message')
            .limit(limit)
            .offset(offset);

        if (key)
            qb.addSelect(raw(`(CASE WHEN chat.key = '${key}' THEN 1 ELSE 0 END) as equal_key`)).andWhere({
                $or: [{ key: { $ilike: `%${key}%` } }, { title: { $ilike: `%${key}%` } }],
            });

        return qb;
    }

    public getUserChats(
        userId: string,
        id?: string,
        key?: string,
        type?: ChatTypesEnum,
        limit?: number,
        offset?: number,
    ): Promise<Chat[]> {
        const equal_key = key ? { equal_key: 'DESC' } : {};

        const qb = this.getChats(key, limit, offset)
            .andWhere({ id, type })
            .addSelect(raw('COALESCE(last_message.created_at, chat.created_at) AS order_created'))
            .leftJoinAndSelect('chat.chatParticipants', 'chat_participants', { 'chat.type': ChatTypesEnum.DIALOG })
            .innerJoinAndSelect('chat.actualChatParticipant', 'actualChatParticipant', { userId })
            .leftJoinAndSelect('chat_participants.user', 'chat_participants_users')
            .orderBy({ ...equal_key, order_created: 'DESC' })
            .groupBy([
                'chat.id',
                'last_message.id',
                'chat_participants.id',
                'actualChatParticipant.id',
                'chat_participants_users.id',
            ]);

        if (key)
            qb.orWhere(key ? { 'users.id': { $ne: userId }, 'users.address': { $ilike: `%${key}%` } } : {})
                .leftJoin('chat.chatParticipants', 'chat_participants_search', {
                    'chat.type': ChatTypesEnum.DIALOG,
                })
                .leftJoin('chat_participants_search.user', 'users');

        return qb.getResult();
    }

    public getDialogByUserIds(senderUserId: string, recipientUserId: string): Promise<Chat | null> {
        return this.getSelectQueryBuilder()
            .innerJoin('chat.chatParticipants', 'chat_participants', {
                userId: senderUserId,
            })
            .innerJoin('chat.chatParticipants', 'chat_participants', {
                userId: recipientUserId,
            })
            .andWhere({ isPublic: false, type: ChatTypesEnum.DIALOG })
            .getSingleResult();
    }

    public getChatByIdWithParticipants(chatId: string): Promise<Chat | null> {
        return this.getChats()
            .andWhere({ id: chatId })
            .innerJoinAndSelect('chat.chatParticipants', 'chat_participants')
            .innerJoinAndSelect('chat_participants.user', 'user')
            .getSingleResult();
    }
}
