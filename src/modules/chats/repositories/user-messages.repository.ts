import { CustomBaseRepository } from '../../../common/mikro-orm/repositories/custom-base.repository';
import { UserMessage } from '../entities/user-message.entity';
import { ChatParticipant } from '../entities/chat-participant.entity';
import { Chat } from '../entities/chat.entity';
import { User } from '../../users/entities/user.entity';
import { Message } from '../entities/message.entity';
import { MessageStatusEnum } from '../types/message-status.enum';

export class UserMessagesRepository extends CustomBaseRepository<UserMessage> {
    protected ALIAS = 'user_message';

    public async createUserMessages(chatId: string, messageId: string, userId: string): Promise<UserMessage[]> {
        const chat = this.em.getReference(Chat, chatId);
        const message = this.em.getReference(Message, messageId);

        await this.updateChatParticipantsIsSentCount(chatId, userId);
        const chatParticipants = await this.em.find<ChatParticipant>(ChatParticipant, { chatId });

        const userMessages = chatParticipants.map(
            ({ userId: id }) =>
                new UserMessage(
                    chat,
                    message,
                    this.em.getReference(User, id),
                    id === userId ? MessageStatusEnum.IS_READ : MessageStatusEnum.IS_SENT,
                ),
        );

        await this.insertMany(userMessages);

        return userMessages;
    }

    public getMessages(chatId: string, userId: string, limit?: number, offset?: number): Promise<UserMessage[]> {
        return this.getSelectQueryBuilder()
            .where({ chatId, userId })
            .innerJoinAndSelect('user_message.message', 'message')
            .limit(limit)
            .offset(offset)
            .getResult();
    }

    private updateChatParticipantsIsSentCount(chatId: string, userId: string) {
        return this.em.execute(`
            UPDATE chat_participants cp
            SET count_is_send_messages = count_is_send_messages+1
            WHERE cp.chat_id = '${chatId}'
            AND cp.user_id != '${userId}'
        `);
    }
}
