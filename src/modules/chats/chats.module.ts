import { forwardRef, Module } from '@nestjs/common';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { SocketIoModule } from '../socket-io/socket-io.module';
import { Envs } from '../../common/envs/env';
import { UsersModule } from '../users/users.module';
import { ChatsService } from './chats.service';
import { Chat } from './entities/chat.entity';
import { ChatParticipant } from './entities/chat-participant.entity';
import { Message } from './entities/message.entity';
import { UserMessage } from './entities/user-message.entity';
import { ChatsController } from './swagger/chats.controller';

@Module({
    imports: [
        MikroOrmModule.forFeature([Chat, ChatParticipant, Message, UserMessage]),
        forwardRef(() => SocketIoModule),
        forwardRef(() => UsersModule),
    ],
    providers: [ChatsService],
    controllers: Envs.swagger.isWriteConfig ? [ChatsController] : [],
    exports: [ChatsService],
})
export class ChatsModule {}
