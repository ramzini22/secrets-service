import { ApiProperty } from '@nestjs/swagger';
import { MessageStatusEnum } from '../../types/message-status.enum';
import { Message } from '../../entities/message.entity';
import { UserMessage } from '../../entities/user-message.entity';

export class MessageDto {
    @ApiProperty({ description: 'id сообщения' })
    readonly id: string;

    @ApiProperty({ description: 'сообщение' })
    readonly encryptMessage: string;

    @ApiProperty({ description: 'статус', enum: MessageStatusEnum, default: MessageStatusEnum.IS_READ })
    readonly status: MessageStatusEnum;

    @ApiProperty({ description: 'дата создания' })
    readonly createdAt: Date;

    @ApiProperty({ description: 'дата обновления' })
    readonly updatedAt: Date;

    constructor(id: string, encryptMessage: string, status: MessageStatusEnum, createdAt: Date, updatedAt: Date) {
        this.id = id;
        this.encryptMessage = encryptMessage;
        this.status = status;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public static getFromMessageEntity(entity: Message): MessageDto {
        const status = entity.userMessages[0]?.status || MessageStatusEnum.IS_READ;

        return new MessageDto(entity.id, entity.encryptMessage, status, entity.createdAt, entity.updatedAt);
    }

    public static getFromUserMessageEntity(entity: UserMessage): MessageDto {
        const message = entity.message;

        return new MessageDto(message.id, message.encryptMessage, entity.status, message.createdAt, message.updatedAt);
    }

    public static getFromMessageEntities(entities: Message[]): MessageDto[] {
        return entities.map((entity) => this.getFromMessageEntity(entity));
    }

    public static getFromUserMessageEntities(entities: UserMessage[]): MessageDto[] {
        return entities.map((entity) => this.getFromUserMessageEntity(entity));
    }
}
