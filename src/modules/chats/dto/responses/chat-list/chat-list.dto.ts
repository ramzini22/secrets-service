import { ApiProperty } from '@nestjs/swagger';
import { Chat } from '../../../entities/chat.entity';
import { MetaDto } from '../../../../../common/dto/responses/meta.dto';
import { ChatDto } from './chat.dto';

export class ChatListDto {
    @ApiProperty({ description: 'мета данные', type: MetaDto })
    readonly meta: MetaDto;

    @ApiProperty({ description: 'даные', type: ChatDto, isArray: true })
    readonly data: ChatDto[];

    constructor(meta: MetaDto, data: ChatDto[]) {
        this.meta = meta;
        this.data = data;
    }

    public static getFromChats(chats: Chat[], count: number): ChatListDto {
        return new ChatListDto(new MetaDto(count), ChatDto.getFromChatEntities(chats));
    }
}
