import { ApiProperty } from '@nestjs/swagger';
import { Chat } from '../../../entities/chat.entity';
import { MessageDto } from '../message.dto';
import { ChatParticipant } from '../../../entities/chat-participant.entity';
import { ChatTypesEnum } from '../../../types/chat-types.enum';

export class ChatDto {
    @ApiProperty({ description: 'id чата' })
    readonly id: string;

    @ApiProperty({ description: 'чата' })
    readonly key: string;

    @ApiProperty({ description: 'открытый чат' })
    readonly isPublic: boolean;

    @ApiProperty({ description: 'аватарка чата', nullable: true })
    readonly avatar: string;

    @ApiProperty({ description: 'название чата', nullable: true })
    readonly title: string;

    @ApiProperty({ description: 'зашифрованный ключ от чата', nullable: true })
    readonly chatEncryptKey: string | null;

    @ApiProperty({ description: 'последнее сообщение', type: MessageDto, nullable: true })
    readonly lastMessage: MessageDto | null;

    @ApiProperty({ description: 'количество непрочитанных сообщений' })
    readonly countIsNotReadMessages: number;

    constructor(
        id: string,
        key: string,
        isPublic: boolean,
        avatar: string,
        title: string,
        chatEncryptKey: string | null,
        lastMessage: MessageDto | null,
        countIsNotReadMessages: number,
    ) {
        this.id = id;
        this.key = key;
        this.isPublic = isPublic;
        this.chatEncryptKey = chatEncryptKey;
        this.avatar = avatar;
        this.title = title;
        this.lastMessage = lastMessage;
        this.countIsNotReadMessages = countIsNotReadMessages;
    }

    public static getFromChatParticipant(entity: ChatParticipant): ChatDto {
        const chat = entity.chat;
        const lastMessage = chat.lastMessage ? MessageDto.getFromMessageEntity(chat.lastMessage) : null;
        const chatEncryptKey = entity?.encryptAesKey ?? null;
        const title = this.getTitleFromChat(entity);

        return new ChatDto(
            chat.id,
            chat.key,
            chat.isPublic,
            chat.avatar,
            title,
            chatEncryptKey,
            lastMessage,
            entity.countIsSendMessages ?? 0,
        );
    }

    public static getFromChatEntity(entity: Chat): ChatDto {
        const chatEncryptKey = entity.actualChatParticipant[0]?.encryptAesKey ?? null;
        const lastMessage = entity.lastMessage ? MessageDto.getFromMessageEntity(entity.lastMessage) : null;
        const title = this.getTitleFromChat(entity.actualChatParticipant[0]);
        const countIsNotReadMessages = entity.actualChatParticipant[0].countIsSendMessages ?? 0;

        return new ChatDto(
            entity.id,
            entity.key,
            entity.isPublic,
            entity.avatar,
            title,
            chatEncryptKey,
            lastMessage,
            countIsNotReadMessages,
        );
    }

    public static getFromChatEntities(entities: Chat[]): ChatDto[] {
        return entities.map((entity) => this.getFromChatEntity(entity));
    }

    private static getTitleFromChat(entity: ChatParticipant): string {
        const chat = entity.chat;
        let title = chat.title;

        if (chat.type === ChatTypesEnum.DIALOG) {
            const chatParticipants = chat.chatParticipants.getItems();

            if (entity.userId === chatParticipants[0].userId) title = chatParticipants[1].user.address;
            else title = chatParticipants[0].user.address;
        }

        return title;
    }
}
