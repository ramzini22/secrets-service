import { IsUUID, MaxLength } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmptyString } from '../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';
import { IsOptional } from '../../../../common/decorators/pipes/validations/is-optional.decorator';

export class CreateMessageDto {
    @ApiProperty({ description: 'encrypt message', maxLength: 32768 })
    @IsNotEmptyString()
    @MaxLength(32768)
    @IsNotEmptyString()
    readonly encryptMessage!: string;

    @ApiPropertyOptional({ description: 'chat ID' })
    @IsUUID()
    @IsOptional()
    readonly chatId!: string;
}
