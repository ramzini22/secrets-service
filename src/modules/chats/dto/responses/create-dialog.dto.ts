import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, MinLength } from 'class-validator';
import { IsNotEmptyString } from '../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';

export class CreateDialogDto {
    @ApiProperty({ description: 'encrypt message', maxLength: 32768 })
    @MaxLength(32768)
    @IsNotEmptyString()
    readonly encryptMessage!: string;

    @ApiProperty({ description: 'ton wallet address', maxLength: 48, minLength: 48 })
    @IsNotEmptyString()
    @MaxLength(48)
    @MinLength(48)
    readonly address!: string;

    @ApiProperty({ description: 'creator generated chat encrypt aes key', maxLength: 4096 })
    @MaxLength(4096)
    @IsNotEmptyString()
    readonly senderEncryptAesKey!: string;

    @ApiProperty({ description: 'recipient generated chat encrypt aes key', maxLength: 4096 })
    @MaxLength(4096)
    @IsNotEmptyString()
    readonly recipientEncryptAesKey!: string;
}
