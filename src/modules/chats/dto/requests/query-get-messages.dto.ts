import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';
import { QueryLimitOffsetDto } from '../../../../common/dto/requests/query-limit-offset.dto';

export class QueryGetMessagesDto extends QueryLimitOffsetDto {
    @ApiProperty({ description: 'id' })
    @IsUUID()
    readonly id!: string;
}
