import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { MaxLength, MinLength } from 'class-validator';
import { IsBooleanStringTransform } from '../../../../common/decorators/pipes/transform/is-boolean-string.decorator';
import { IsOptional } from '../../../../common/decorators/pipes/validations/is-optional.decorator';
import { IsNotEmptyString } from '../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';

export class CreateChatDto {
    @ApiPropertyOptional({ maxLength: 256, minLength: 3 })
    @MinLength(3)
    @MaxLength(256)
    @IsNotEmptyString()
    @IsOptional()
    readonly key!: string;

    @ApiPropertyOptional({ default: true })
    @IsOptional()
    @IsBooleanStringTransform()
    readonly isPublic!: boolean;

    @ApiProperty({ maxLength: 256, minLength: 3 })
    @MinLength(3)
    @MaxLength(256)
    @IsNotEmptyString()
    readonly title?: string;

    @ApiProperty({ maxLength: 4096, minLength: 256 })
    @IsNotEmptyString()
    @MaxLength(4096)
    @MinLength(256)
    readonly encryptAesKey!: string;
}
