import { IsEnum } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmptyString } from '../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';
import { IsOptional } from '../../../../common/decorators/pipes/validations/is-optional.decorator';
import { ChatTypesEnum } from '../../types/chat-types.enum';
import { QueryIdLimitOffsetDto } from '../../../../common/dto/requests/query-id-limit-offset.dto';

export class QueryGetChatsDto extends QueryIdLimitOffsetDto {
    @ApiPropertyOptional({ description: 'ключ, по которому происходит поиск чата' })
    @IsNotEmptyString()
    @IsOptional()
    readonly key?: string;

    @ApiPropertyOptional({ description: 'chat status', enum: ChatTypesEnum })
    @IsEnum(ChatTypesEnum)
    @IsOptional()
    readonly type?: ChatTypesEnum;
}
