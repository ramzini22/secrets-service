import { createCustomBadRequestError } from '../../../common/utils/error.utils';

export class ChatsExceptions {
    static isNotParticipant(chatId: string) {
        return createCustomBadRequestError(`user is not chat participant with index = ${chatId}.`);
    }

    static notFound(chatId: string) {
        return createCustomBadRequestError(`chat ${chatId} not found.`);
    }
}
