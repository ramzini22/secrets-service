export enum ChatTypesEnum {
    MONOLOG = 'monolog',
    DIALOG = 'dialog',
    CHAT = 'chat',
}
