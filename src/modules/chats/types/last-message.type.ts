export type LastMessageType = {
    readonly id: string;
    readonly value: string;
    readonly createdAt: Date;
    readonly updatedAt: Date;
};
