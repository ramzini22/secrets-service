import { Controller, Get } from '@nestjs/common';
import { CustomSubscribeMessage } from '../../../common/decorators/metadata/custom-subscrube-message/custom-subscribe-message.decorator';
import { ApiSendResponse } from '../../../common/decorators/swagger/api-send-response.decorator';
import { ChatDto } from '../dto/responses/chat-list/chat.dto';

@Controller()
export class ChatsController {
    @ApiSendResponse(ChatDto, 'обновления чатов')
    @CustomSubscribeMessage('chats:listen', Get)
    updateChats() {}
}
