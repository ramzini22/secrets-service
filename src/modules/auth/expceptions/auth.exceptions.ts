import { createCustomBadRequestError } from '../../../common/utils/error.utils';
import { Envs } from '../../../common/envs/env';

export class AuthExceptions {
    static invalidPayloadLength(length: number) {
        return createCustomBadRequestError(`invalid payload length, got ${length}, expected 32`);
    }

    static invalidPayloadSignature() {
        return createCustomBadRequestError('invalid payload signature');
    }

    static payloadExpired() {
        return createCustomBadRequestError('payload expired');
    }

    static tonProofExpired() {
        return createCustomBadRequestError('ton proof has been expired');
    }

    static invalidDomain(domain: string) {
        return createCustomBadRequestError(`wrong domain, got ${domain}, expected ${Envs.main.domain}`);
    }

    static invalidDomainLengthBytes(lengthBytes: number) {
        return createCustomBadRequestError(`domain length mismatched against provided length bytes of ${lengthBytes}`);
    }

    static verificationFailed() {
        return createCustomBadRequestError('verification failed');
    }

    static unsupportedWalletVersion() {
        return createCustomBadRequestError('unsupported wallet version');
    }
}
