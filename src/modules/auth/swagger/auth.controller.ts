import { Controller, Get } from '@nestjs/common';
import { AuthTokenDto } from '../dto/responses/auth-token.dto';
import { ApiSendResponse } from '../../../common/decorators/swagger/api-send-response.decorator';
import { CustomSubscribeMessage } from '../../../common/decorators/metadata/custom-subscrube-message/custom-subscribe-message.decorator';

@Controller()
export class AuthController {
    @ApiSendResponse(AuthTokenDto, 'изменения токена авторизации')
    @CustomSubscribeMessage('auth:refresh:listen', Get)
    updateAuthToken() {}
}
