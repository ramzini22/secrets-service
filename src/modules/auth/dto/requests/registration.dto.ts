import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, MinLength } from 'class-validator';
import { IsNotEmptyString } from '../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';

export class RegistrationDto {
    @ApiProperty({ description: 'токен индентификации кошелька', minLength: 32, maxLength: 1024 })
    @IsNotEmptyString()
    @MinLength(32)
    @MaxLength(1024)
    readonly payloadToken!: string;

    @ApiProperty({ description: 'публичный ключ', minLength: 512, maxLength: 1024 })
    @IsNotEmptyString()
    @MinLength(512)
    @MaxLength(1024)
    readonly publicKey!: string;

    @ApiProperty({ description: 'приватный ключ в зашифрованном виде', minLength: 512, maxLength: 1024 })
    @IsNotEmptyString()
    @MinLength(8192)
    @MaxLength(16384)
    readonly encryptPrivateKey!: string;

    @ApiProperty({ description: 'хэш секретной фразы', maxLength: 1024 })
    @IsNotEmptyString()
    @MaxLength(1024)
    readonly hashPassphrase!: string;

    @ApiProperty({ description: 'хэш user-agent', minLength: 32, maxLength: 1024 })
    @IsNotEmptyString()
    @MinLength(32)
    @MaxLength(1024)
    readonly hashUserAgent!: string;
}
