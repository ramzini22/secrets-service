import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, MinLength } from 'class-validator';

export class ConnectDto {
    @ApiProperty({ description: 'токен авторизации', maxLength: 512, minLength: 256 })
    @MaxLength(512)
    @MinLength(256)
    public authToken!: string;

    @ApiProperty({ description: 'хэш секретной фразы', minLength: 32, maxLength: 1024 })
    @MinLength(32)
    @MaxLength(1024)
    readonly hashPassphrase!: string;

    @ApiProperty({ description: 'хэш user-agent', minLength: 32, maxLength: 1024 })
    @MinLength(32)
    @MaxLength(1024)
    readonly hashUserAgent!: string;
}
