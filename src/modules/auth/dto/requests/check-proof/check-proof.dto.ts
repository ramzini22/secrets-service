import { IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ChainEnum } from '../../../types/chain.enum';
import { IsNotEmptyString } from '../../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';
import { IsObjectOfType } from '../../../../../common/decorators/pipes/validations/is-object-of-type.decorator';
import { TonProofDto } from './ton-proof.dto';

export class CheckProofDto {
    @ApiProperty({ description: 'адрес кошелька' })
    @IsNotEmptyString()
    readonly address!: string;

    @ApiProperty({ description: 'тип сети', enum: ChainEnum })
    @IsEnum(ChainEnum)
    readonly network!: ChainEnum;

    @ApiProperty({ description: 'доказательство подлинности кошелька', type: TonProofDto })
    @IsObjectOfType(TonProofDto)
    readonly proof!: TonProofDto;
}
