import { IsInt } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from '../../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';

export class TonDomainDto {
    @ApiProperty({ description: 'длина байтов' })
    @IsInt()
    readonly lengthBytes!: number;

    @ApiProperty({ description: 'значение' })
    @IsNotEmptyString()
    readonly value!: string;
}
