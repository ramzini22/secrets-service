import { IsInt } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmptyString } from '../../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';
import { IsObjectOfType } from '../../../../../common/decorators/pipes/validations/is-object-of-type.decorator';
import { TonDomainDto } from './ton-domain.dto';

export class TonProofDto {
    @ApiProperty({ description: 'домен', type: TonDomainDto })
    @IsObjectOfType(TonDomainDto)
    readonly domain!: TonDomainDto;

    @ApiProperty({ description: 'передаваемый payload' })
    @IsNotEmptyString()
    readonly payload!: string;

    @ApiProperty({ description: 'сигнатура' })
    @IsNotEmptyString()
    readonly signature!: string;

    @ApiProperty({ description: 'первоначальное состояние' })
    @IsNotEmptyString()
    readonly state_init!: string;

    @ApiProperty({ description: 'время создания в секундах' })
    @IsInt()
    readonly timestamp!: number;
}
