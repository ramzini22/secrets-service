import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, MinLength } from 'class-validator';

export class DeleteSessionsDto {
    @ApiProperty({ isArray: true, type: String, description: 'array of hash user-agent' })
    @MaxLength(1024, { each: true })
    @MinLength(256, { each: true })
    readonly encryptUserAgents!: string[];
}
