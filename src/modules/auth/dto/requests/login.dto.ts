import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, MinLength } from 'class-validator';
import { IsNotEmptyString } from '../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';

export class LoginDto {
    @ApiProperty({ description: 'токен индентификации кошелька', minLength: 32, maxLength: 1024 })
    @IsNotEmptyString()
    @MaxLength(1024)
    @MinLength(32)
    readonly payloadToken!: string;

    @ApiProperty({ description: 'хэш секретной фразы', minLength: 32, maxLength: 1024 })
    @IsNotEmptyString()
    @MinLength(32)
    @MaxLength(1024)
    readonly hashPassphrase!: string;

    @ApiProperty({ description: 'хэш user-agent', minLength: 32, maxLength: 1024 })
    @IsNotEmptyString()
    @MinLength(32)
    @MaxLength(1024)
    readonly hashUserAgent!: string;
}
