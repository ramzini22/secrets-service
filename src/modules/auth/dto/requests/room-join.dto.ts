import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, MaxLength, MinLength } from 'class-validator';
import { RoomsEnum } from '../../types/rooms.enum';
import { IsNotEmptyString } from '../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';

export class RoomJoinDto {
    @ApiProperty({ description: 'room', enum: RoomsEnum })
    @IsEnum(RoomsEnum)
    readonly room!: RoomsEnum;

    @ApiProperty({ description: 'id of room', maxLength: 48, minLength: 3 })
    @IsNotEmptyString()
    @MaxLength(48)
    @MinLength(3)
    readonly id!: string;
}
