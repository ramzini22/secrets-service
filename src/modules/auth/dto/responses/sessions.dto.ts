import { ApiProperty } from '@nestjs/swagger';
import { SessionDto } from './session.dto';

export class SessionsDto {
    @ApiProperty({ description: 'session data', type: SessionDto, isArray: true })
    readonly sessions: SessionDto[];

    constructor(sessions: SessionDto[]) {
        this.sessions = sessions;
    }
}
