import { ApiProperty } from '@nestjs/swagger';

export class AuthDto {
    @ApiProperty({ description: 'токен авторизации' })
    readonly authToken: string;

    @ApiProperty({ description: 'приватный ключ в зашифрованном виде' })
    readonly encryptPrivateKey: string;

    constructor(authToken: string, encryptPrivateKey: string) {
        this.authToken = authToken;
        this.encryptPrivateKey = encryptPrivateKey;
    }
}
