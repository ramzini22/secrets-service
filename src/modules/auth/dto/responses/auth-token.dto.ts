import { ApiProperty } from '@nestjs/swagger';

export class AuthTokenDto {
    @ApiProperty({ description: 'токен авторизации' })
    readonly authToken!: string;

    constructor(authToken: string) {
        this.authToken = authToken;
    }
}
