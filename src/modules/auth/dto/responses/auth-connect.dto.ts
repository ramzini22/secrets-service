import { ApiProperty } from '@nestjs/swagger';

export class AuthConnectDto {
    @ApiProperty({ description: 'authorization token' })
    readonly authToken!: string;

    constructor(authToken: string) {
        this.authToken = authToken;
    }
}
