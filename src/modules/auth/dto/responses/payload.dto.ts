import { ApiProperty } from '@nestjs/swagger';

export class PayloadDto {
    @ApiProperty({ description: 'ключ для входа в ton кошелек' })
    readonly payload: string;

    constructor(payload: string) {
        this.payload = payload;
    }
}
