import { ApiProperty } from '@nestjs/swagger';

export class SessionDto {
    @ApiProperty({ description: 'encrypt user-agent' })
    readonly encryptUserAgent: string;

    @ApiProperty({ description: 'is online or not' })
    readonly isOnline: boolean;

    @ApiProperty({ description: 'update date' })
    readonly updatedAt: Date;

    constructor(encryptUserAgent: string, isOnline: boolean, updatedAt: Date) {
        this.encryptUserAgent = encryptUserAgent;
        this.isOnline = isOnline;
        this.updatedAt = updatedAt;
    }
}
