import { ApiProperty } from '@nestjs/swagger';

export class TokenDto {
    @ApiProperty({ description: 'токен для авторизации' })
    readonly payloadToken!: string;

    @ApiProperty({ description: 'приватный ключ пользователя в зашифрованном виде', nullable: true })
    readonly encryptPrivateKey: string | null;

    constructor(payloadToken: string, encryptPrivateKey?: string) {
        this.payloadToken = payloadToken;
        this.encryptPrivateKey = encryptPrivateKey ?? null;
    }
}
