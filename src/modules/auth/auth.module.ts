import { forwardRef, Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { Envs } from '../../common/envs/env';
import { CachesModule } from '../caches/caches.module';
import { SocketIoModule } from '../socket-io/socket-io.module';
import { AuthService } from './services/auth.service';
import { TonService } from './services/ton.service';
import { AuthController } from './swagger/auth.controller';

@Module({
    imports: [UsersModule, CachesModule, forwardRef(() => SocketIoModule)],
    providers: [AuthService, TonService],
    exports: [AuthService],
    controllers: Envs.swagger.isWriteConfig ? [AuthController] : [],
})
export class AuthModule {}
