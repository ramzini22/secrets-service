import { Injectable } from '@nestjs/common';
import jwt from 'jsonwebtoken';
import { Socket } from 'socket.io';
import { Envs } from '../../../common/envs/env';
import { DataResponse } from '../../../common/dto/responses/data-response';
import { PayloadDto } from '../dto/responses/payload.dto';
import { CheckProofDto } from '../dto/requests/check-proof/check-proof.dto';
import { TokenDto } from '../dto/responses/token.dto';
import { UsersRepository } from '../../users/repositories/users.repository';
import { AuthDto } from '../dto/responses/auth.dto';
import { PayloadType } from '../types/payload.type';
import { AuthExceptions } from '../expceptions/auth.exceptions';
import { User } from '../../users/entities/user.entity';
import { AuthTokenDto } from '../dto/responses/auth-token.dto';
import { UsersConnectionsService } from '../../socket-io/raw/users-connections.service';
import { AuthConnectDto } from '../dto/responses/auth-connect.dto';
import { socketServer } from '../../socket-io/raw/socket-server';
import { SocketIoServer } from '../../socket-io/services/socket-io.server';
import { SessionsDto } from '../dto/responses/sessions.dto';
import { DateUtils } from '../../../common/utils/date.utils';
import { RoomsEnum } from '../types/rooms.enum';
import { CustomRequestType } from '../../../common/types/custom-request.type';
import { UserListenDto } from '../../users/dto/responses/user-listen.dto';
import { TonService } from './ton.service';

@Injectable()
export class AuthService {
    constructor(
        private readonly tonService: TonService,
        private readonly usersRepository: UsersRepository,
        private readonly usersConnectionsService: UsersConnectionsService,
        private readonly socketIoServer: SocketIoServer,
    ) {}
    public getPayload(): DataResponse<PayloadDto> {
        const response = new DataResponse<PayloadDto>();

        const payloadHex = this.tonService.generatePayload();

        return response.setData(new PayloadDto(payloadHex));
    }

    public async checkProof(body: CheckProofDto): Promise<DataResponse<TokenDto>> {
        const response = new DataResponse<TokenDto>();
        const parsedAddress = await this.tonService.getAddress(body);

        const user = await this.usersRepository.findOne({ address: parsedAddress }, { populate: ['keyPair'] });

        const token = this.generatePayloadToken(parsedAddress);

        return response.setData(new TokenDto(token, user?.keyPair?.encryptPrivateKey));
    }

    public async registration(
        socketId: string,
        payloadToken: string,
        hashPassphrase: string,
        hashUserAgent: string,
        publicKey: string,
        encryptPrivateKey: string,
    ): Promise<DataResponse<AuthTokenDto>> {
        let payload: PayloadType;

        try {
            payload = jwt.verify(payloadToken, Envs.main.sharedSecret) as PayloadType;
        } catch (exception) {
            throw AuthExceptions.payloadExpired();
        }

        const userIsExist = await this.usersRepository.count({ address: payload.address });

        if (userIsExist) throw AuthExceptions.verificationFailed();

        const user = await this.usersRepository.createUser(
            payload.address,
            hashPassphrase,
            publicKey,
            encryptPrivateKey,
        );

        const response = this.generateAuthToken(user);
        const authToken = response.getData()?.authToken;

        if (!authToken) throw AuthExceptions.payloadExpired();

        await this.usersConnectionsService.addSession(socketId, hashUserAgent, authToken, user);
        await this.updateUserSessionsBySocketId(socketId);
        await this.updateIsOlineUser(user.id);

        return response;
    }

    public async login(
        socketId: string,
        payloadToken: string,
        hashPassphrase: string,
        userAgent: string,
    ): Promise<DataResponse<AuthDto>> {
        let payload: PayloadType;

        try {
            payload = jwt.verify(payloadToken, Envs.main.sharedSecret) as PayloadType;
        } catch (exception) {
            throw AuthExceptions.payloadExpired();
        }

        const user = await this.usersRepository.findOne(
            { address: payload.address, hashPassphrase },
            { populate: ['keyPair'] },
        );

        if (!user) throw AuthExceptions.tonProofExpired();

        const response = this.generateAuthToken(user);
        const authToken = response.getData()?.authToken;

        if (authToken) await this.usersConnectionsService.addSession(socketId, userAgent, authToken, user);
        else throw AuthExceptions.tonProofExpired();

        await this.updateUserSessionsBySocketId(socketId);
        await this.updateIsOlineUser(user.id);

        return new DataResponse<AuthDto>(new AuthDto(authToken, user.keyPair.encryptPrivateKey));
    }

    public async logout(socketId: string, userId: string): Promise<DataResponse<object>> {
        const response = new DataResponse<object>();

        await this.usersConnectionsService.deleteSession(socketId);
        await this.updateUserSessionsByUserId(userId);
        await this.updateIsOlineUser(userId);

        return response.setSuccess(true);
    }

    public async addUserConnection(
        request: CustomRequestType,
        socketId: string,
        authToken: string,
        hashPassphrase: string,
        hashUserAgent: string,
    ): Promise<DataResponse<AuthConnectDto>> {
        const response = new DataResponse<AuthConnectDto>();
        const session = await this.usersConnectionsService.getSession(authToken);

        if (!session || session.user.hashPassphrase !== hashPassphrase) throw AuthExceptions.payloadExpired();

        try {
            jwt.verify(authToken, Envs.main.sharedSecret);

            const data = await this.usersConnectionsService.reconnectSession(
                socketId,
                hashUserAgent,
                authToken,
                session.user,
            );

            response.setData(data);
        } catch (exception) {
            const authData = this.generateAuthToken(session.user);
            const authTokenNew = authData.getData()?.authToken;

            if (!authTokenNew) throw AuthExceptions.payloadExpired();

            const data = await this.usersConnectionsService.reconnectSession(
                socketId,
                hashUserAgent,
                authToken,
                session.user,
                authTokenNew,
            );

            response.setData(data);

            socketServer
                .to(socketId)
                .emit('auth:refresh:listen', new DataResponse<AuthTokenDto>(new AuthTokenDto(authTokenNew)));
        }

        request.userId = session.user.id;
        await this.updateUserSessionsBySocketId(socketId);
        await this.updateIsOlineUser(session.user.id);

        return response;
    }

    public async deleteUserConnection(socketId: string): Promise<void> {
        const user = this.usersConnectionsService.getUserBySocketId(socketId);

        this.usersConnectionsService.disconnectUser(socketId);

        if (user) {
            await this.updateUserSessionsByUserId(user.id);
            await this.updateIsOlineUser(user.id);
        }
    }

    public async deleteSessions(
        socketId: string,
        userId: string,
        encryptUserAgents: string[],
    ): Promise<DataResponse<SessionsDto>> {
        const socketIds = this.usersConnectionsService.getUserConnectionsBySocketId(socketId);

        const sessions = await this.usersConnectionsService.deleteSessions(userId, encryptUserAgents);
        this.socketIoServer.emit(socketIds, 'sessions:listen', sessions);

        await this.updateIsOlineUser(userId);

        return sessions;
    }

    public joinRoom(client: Socket, room: RoomsEnum, id: string): void {
        const roomName = `${room}:${id}`;

        if ([RoomsEnum.USERS].includes(room)) this.socketIoServer.joinRoom(client, roomName);
    }

    public leaveRoom(client: Socket, room: RoomsEnum, id: string): void {
        const roomName = `${room}:${id}`;

        if ([RoomsEnum.USERS].includes(room)) this.socketIoServer.leaveRoom(client, roomName);
    }

    private generatePayloadToken(address: string): string {
        const now = Math.floor(Date.now() / 1000);

        const claims = {
            exp: now + Envs.main.payloadTtl,
            address,
        };

        return jwt.sign(claims, Envs.main.sharedSecret);
    }

    private generateAuthToken(user: User): DataResponse<AuthTokenDto> {
        const response = new DataResponse<AuthTokenDto>();

        const authToken = jwt.sign(
            {
                address: user.address,
                hashPassphrase: user.hashPassphrase,
            },
            Envs.main.sharedSecret,
        );

        return response.setData(new AuthTokenDto(authToken));
    }

    private async updateUserSessionsByUserId(userId: string): Promise<DataResponse<SessionsDto>> {
        const response = await this.usersConnectionsService.getSessionsByUserId(userId);
        this.socketIoServer.emitAllUserDevicesByUserId(userId, 'sessions:listen', response);

        return response;
    }

    private async updateUserSessionsBySocketId(socketId: string): Promise<DataResponse<SessionsDto>> {
        const response = await this.usersConnectionsService.getSessionsBySocketId(socketId);
        this.socketIoServer.emitAllUserDevicesBySocketId(socketId, 'sessions:listen', response);

        return response;
    }

    private async updateIsOlineUser(userId: string): Promise<void> {
        const user = await this.usersRepository.findOne({ id: userId });
        const userConnections = this.usersConnectionsService.getUserConnectionsByUserId(userId);

        if (!user) return;

        if (user.isOnline && userConnections?.length > 0) return;

        if (!user.isOnline && userConnections?.length == 0) return;

        const updatedUser = await this.usersRepository.updateOne(
            { id: userId },
            { isOnline: !user.isOnline, lastOnlineAt: DateUtils.now() },
        );

        if (!updatedUser) return;

        this.socketIoServer.emitRoom(
            `users:${user.address}`,
            new DataResponse<UserListenDto>(UserListenDto.getFromUserEntity(updatedUser)),
        );
    }
}
