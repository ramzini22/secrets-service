import { TonDomainType } from './ton-domain.type';

export type TonProofType = {
    readonly domain: TonDomainType;
    readonly payload: string;
    readonly signature: string;
    readonly state_init: string;
    readonly timestamp: number;
};
