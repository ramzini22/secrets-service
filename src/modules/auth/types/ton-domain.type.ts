export type TonDomainType = {
    readonly lengthBytes: number;
    readonly value: string;
};
