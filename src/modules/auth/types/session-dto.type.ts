import { SessionType } from '../../caches/sessions/session.type';

export type SessionDtoType = SessionType;
