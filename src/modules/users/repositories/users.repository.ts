import { CustomBaseRepository } from '../../../common/mikro-orm/repositories/custom-base.repository';
import { User } from '../entities/user.entity';
import { UserKeyPair } from '../entities/user-key-pair.entity';

export class UsersRepository extends CustomBaseRepository<User> {
    protected ALIAS = 'users';

    public async createUser(
        address: string,
        hashPassphrase: string,
        publicKey: string,
        encryptPrivateKey: string,
    ): Promise<User> {
        const user = new User({ address, hashPassphrase });
        await this.insert(user);
        const userKeyPair = new UserKeyPair({ publicKey, encryptPrivateKey, user });
        await this.em.insert<UserKeyPair>(userKeyPair);

        return userKeyPair.user;
    }

    public getUsers(address?: string, limit?: number, offset?: number): Promise<[User[], number]> {
        return this.getSelectQueryBuilder()
            .andWhere({ address })
            .innerJoinAndSelect('users.keyPair', 'users_key_pair')
            .limit(limit)
            .offset(offset)
            .getResultAndCount();
    }
}
