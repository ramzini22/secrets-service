import { Module } from '@nestjs/common';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Envs } from '../../common/envs/env';
import { User } from './entities/user.entity';
import { UserKeyPair } from './entities/user-key-pair.entity';
import { UsersController } from './swagger/users.controller';
import { UsersService } from './users.service';

@Module({
    imports: [MikroOrmModule.forFeature([User, UserKeyPair])],
    providers: [UsersService],
    exports: [MikroOrmModule, UsersService],
    controllers: Envs.swagger.isWriteConfig ? [UsersController] : [],
})
export class UsersModule {}
