import { createCustomBadRequestError } from '../../../common/utils/error.utils';

export class UsersExceptions {
    static isNotFoundUser(address: string) {
        return createCustomBadRequestError(`user is not found with address = ${address}.`);
    }
}
