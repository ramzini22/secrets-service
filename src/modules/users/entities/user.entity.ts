import { Entity, Index, OneToOne, Property } from '@mikro-orm/core';
import { CreatedUpdatedEntity } from '../../../common/mikro-orm/entities/created-updated.entity';
import { UsersRepository } from '../repositories/users.repository';
import { UserKeyPair } from './user-key-pair.entity';

@Entity({ tableName: 'users', repository: () => UsersRepository })
@Index({ properties: ['address'] })
export class User extends CreatedUpdatedEntity {
    @Property({ unique: true })
    readonly address!: string;

    @Property()
    readonly hashPassphrase!: string;

    @Property({ default: true })
    readonly isOnline!: boolean;

    @Property({ defaultRaw: 'NOW()' })
    readonly lastOnlineAt!: Date;

    @OneToOne(() => UserKeyPair, (keyPair) => keyPair.user)
    readonly keyPair!: UserKeyPair;

    constructor(columns: Partial<User>) {
        super();
        Object.assign(this, columns);
    }
}
