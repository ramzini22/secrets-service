import { Entity, Index, OneToOne, Property } from '@mikro-orm/core';
import { CreatedUpdatedEntity } from '../../../common/mikro-orm/entities/created-updated.entity';
import { User } from './user.entity';

@Entity({ tableName: 'users_keys_pairs' })
@Index({ properties: ['userId'] })
export class UserKeyPair extends CreatedUpdatedEntity {
    @Property({ persist: false })
    readonly userId!: string;

    @Property({ columnType: 'varchar(1024)' })
    readonly publicKey!: string;

    @Property({ columnType: 'varchar(16384)' })
    readonly encryptPrivateKey!: string;

    constructor(columns: Partial<UserKeyPair>) {
        super();
        Object.assign(this, columns);
    }

    @OneToOne(() => User)
    readonly user!: User;
}
