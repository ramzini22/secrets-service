import { Controller, Get } from '@nestjs/common';
import { CustomSubscribeMessage } from '../../../common/decorators/metadata/custom-subscrube-message/custom-subscribe-message.decorator';
import { ApiSendResponse } from '../../../common/decorators/swagger/api-send-response.decorator';
import { SessionsDto } from '../../auth/dto/responses/sessions.dto';
import { UserListenDto } from '../dto/responses/user-listen.dto';

@Controller()
export class UsersController {
    @ApiSendResponse(SessionsDto, 'изменения информации о сессиях пользователя')
    @CustomSubscribeMessage('sessions:listen', Get)
    updateSessions() {}

    @ApiSendResponse(UserListenDto, 'изменения информации о пользователе')
    @CustomSubscribeMessage('users:{address}:listen', Get)
    updateUser() {}
}
