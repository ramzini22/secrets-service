import { ApiPropertyOptional } from '@nestjs/swagger';
import { MaxLength, MinLength } from 'class-validator';
import { IsOptional } from '../../../../common/decorators/pipes/validations/is-optional.decorator';
import { QueryLimitOffsetDto } from '../../../../common/dto/requests/query-limit-offset.dto';
import { IsNotEmptyString } from '../../../../common/decorators/pipes/validations/is-not-empty-string.decorator';

export class GetUsersDto extends QueryLimitOffsetDto {
    @ApiPropertyOptional({ description: 'ton wallet address', maxLength: 48, minLength: 48 })
    @IsOptional()
    @IsNotEmptyString()
    @MinLength(48)
    @MaxLength(48)
    readonly address!: string;
}
