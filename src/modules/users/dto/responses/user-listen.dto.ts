import { ApiProperty } from '@nestjs/swagger';
import { User } from '../../entities/user.entity';

export class UserListenDto {
    @ApiProperty({ description: 'is online' })
    readonly isOnline: boolean;

    @ApiProperty({ description: 'last online time' })
    readonly lastOnlineAt: Date;

    constructor(isOnline: boolean, lastOnlineAt: Date) {
        this.isOnline = isOnline;
        this.lastOnlineAt = lastOnlineAt;
    }

    public static getFromUserEntity(entity: User): UserListenDto {
        return new UserListenDto(entity.isOnline, entity.lastOnlineAt);
    }

    public static getFromUserEntities(entities: User[]): UserListenDto[] {
        return entities.map((entity) => this.getFromUserEntity(entity));
    }
}
