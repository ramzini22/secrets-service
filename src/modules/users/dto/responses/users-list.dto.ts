import { ApiProperty } from '@nestjs/swagger';
import { MetaDto } from '../../../../common/dto/responses/meta.dto';
import { UserDto } from './user.dto';

export class UsersListDto {
    @ApiProperty({ description: 'meta data', type: MetaDto })
    readonly meta: MetaDto;

    @ApiProperty({ description: 'data', type: UserDto, isArray: true })
    readonly data: UserDto[];

    constructor(meta: MetaDto, data: UserDto[]) {
        this.meta = meta;
        this.data = data;
    }
}
