import { ApiProperty } from '@nestjs/swagger';
import { User } from '../../entities/user.entity';

export class UserDto {
    @ApiProperty({ description: 'ton wallet address' })
    readonly address: string;

    @ApiProperty({ description: 'public key' })
    readonly publicKey: string;

    @ApiProperty({ description: 'is online' })
    readonly isOnline: boolean;

    @ApiProperty({ description: 'last online time' })
    readonly lastOnlineAt: Date;

    constructor(address: string, publicKey: string, isOnline: boolean, lastOnlineAt: Date) {
        this.address = address;
        this.publicKey = publicKey;
        this.isOnline = isOnline;
        this.lastOnlineAt = lastOnlineAt;
    }

    static getFromUserEntity(entity: User): UserDto {
        const publicKey = entity.keyPair.publicKey;

        return new UserDto(entity.address, publicKey, entity.isOnline, entity.lastOnlineAt);
    }

    static getFromUserEntities(entities: User[]): UserDto[] {
        return entities.map((entity) => this.getFromUserEntity(entity));
    }
}
