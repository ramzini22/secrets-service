import { Injectable } from '@nestjs/common';
import { DataResponse } from '../../common/dto/responses/data-response';
import { MetaDto } from '../../common/dto/responses/meta.dto';
import { UsersListDto } from './dto/responses/users-list.dto';
import { UsersRepository } from './repositories/users.repository';
import { UserDto } from './dto/responses/user.dto';

@Injectable()
export class UsersService {
    constructor(private readonly usersRepository: UsersRepository) {}

    public async getUser(address?: string, limit?: number, offset?: number): Promise<DataResponse<UsersListDto>> {
        const response = new DataResponse<UsersListDto>();

        const [data, count] = await this.usersRepository.getUsers(address, limit, offset);
        const metaData = new UsersListDto(new MetaDto(count), UserDto.getFromUserEntities(data));

        return response.setData(metaData);
    }
}
