import { Module } from '@nestjs/common';
import { UsersConnectionsService } from '../socket-io/raw/users-connections.service';
import { SessionsRedis } from './sessions/sessions.redis';
import { UserSessionsRedis } from './user-sessions/user-sessions.redis';

@Module({
    providers: [SessionsRedis, UserSessionsRedis, UsersConnectionsService],
    exports: [SessionsRedis, UserSessionsRedis, UsersConnectionsService],
})
export class CachesModule {}
