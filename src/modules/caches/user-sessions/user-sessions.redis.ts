import { Injectable } from '@nestjs/common';
import { RedisClass } from '../redis';
import { SessionType } from '../sessions/session.type';
import { User } from '../../users/entities/user.entity';
import { SessionsRedis } from '../sessions/sessions.redis';

// userId->SessionType[]
@Injectable()
export class UserSessionsRedis extends RedisClass<SessionType[]> {
    constructor(private readonly sessionsRedis: SessionsRedis) {
        super('user-sessions');
    }

    public async addSession(hashAuthToken: string, user: User, hashUserAgent: string): Promise<SessionType> {
        const session = await this.sessionsRedis.createSession(hashAuthToken, user, hashUserAgent);
        const userSessions = await this.get(user.id);

        if (!userSessions) {
            await this.set(user.id, [session]);

            return session;
        }

        userSessions.push(session);

        await this.set(user.id, userSessions);

        return session;
    }

    public async deleteSession(hashAuthToken: string): Promise<SessionType[]> {
        const session = await this.sessionsRedis.get(hashAuthToken);

        if (!session) return [];

        const userId = session.user.id;
        const userSessions = await this.get(userId);

        if (userSessions) {
            const newUserSessions = userSessions.filter(({ hashAuthToken: authToken }) => authToken !== hashAuthToken);

            if (newUserSessions?.length) await this.set(userId, newUserSessions);
            else await this.delete(userId);

            await this.sessionsRedis.delete(hashAuthToken);

            return newUserSessions;
        }

        return [];
    }

    public async updateSession(
        hashAuthTokenOld: string,
        hashAuthTokenNew: string,
        user: User,
        hashUserAgent: string,
    ): Promise<SessionType> {
        await this.deleteSession(hashAuthTokenOld);

        return this.addSession(hashAuthTokenNew, user, hashUserAgent);
    }

    public async getSession(hashAuthToken: string): Promise<SessionType | null> {
        return this.sessionsRedis.get(hashAuthToken);
    }
}
