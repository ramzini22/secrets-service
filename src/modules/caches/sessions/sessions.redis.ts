import { Injectable } from '@nestjs/common';
import { RedisClass } from '../redis';
import { User } from '../../users/entities/user.entity';
import { DateUtils } from '../../../common/utils/date.utils';
import { SessionType } from './session.type';

// hashAuthToken->SessionType
@Injectable()
export class SessionsRedis extends RedisClass<SessionType> {
    constructor() {
        super('sessions');
    }
    public async createSession(hashAuthToken: string, user: User, encryptUserAgent: string): Promise<SessionType> {
        const session = { encryptUserAgent, hashAuthToken, user, updatedAt: DateUtils.now() };

        await this.set(hashAuthToken, session);

        return session;
    }

    public async deleteSession(hashAuthToken: string): Promise<number> {
        return this.delete(hashAuthToken);
    }
}
