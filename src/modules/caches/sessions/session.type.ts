import { User } from '../../users/entities/user.entity';

export class SessionType {
    readonly user!: User;
    readonly encryptUserAgent!: string;
    readonly hashAuthToken!: string;
    readonly updatedAt!: Date;
}
