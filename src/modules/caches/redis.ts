import { Redis } from 'ioredis';
import { Envs } from '../../common/envs/env';

const redisConnection = new Redis({ port: Envs.redis.port, host: Envs.redis.host, db: Envs.redis.db });

export class RedisClass<T> {
    private readonly beginKey: string;

    constructor(beginKey: string) {
        this.beginKey = beginKey ?? '';
    }

    public async get(key: string): Promise<T | null> {
        const str = await redisConnection.get(`${this.beginKey}::${key}`);

        return str ? <T>JSON.parse(str) : null;
    }

    public async set(key: string, data: T): Promise<'OK'> {
        return redisConnection.set(`${this.beginKey}::${key}`, JSON.stringify(data));
    }

    public async delete(key: string): Promise<number> {
        return redisConnection.del(`${this.beginKey}::${key}`);
    }
}
