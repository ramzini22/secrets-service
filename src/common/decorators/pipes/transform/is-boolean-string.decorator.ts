import { IsBoolean } from 'class-validator';
import { applyDecorators } from '@nestjs/common';
import { Transform } from 'class-transformer';
import { BooleanUtils } from '../../../utils/boolean.utils';

export const IsBooleanStringTransform = () => {
    return applyDecorators(
        Transform((target) => BooleanUtils.isArgumentTrue(target.value)),
        IsBoolean(),
    );
};
