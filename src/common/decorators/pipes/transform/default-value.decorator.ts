import { applyDecorators } from '@nestjs/common';
import { Expose, Transform } from 'class-transformer';

export const Default = (defaultValue: string | number | boolean | Date) => {
    return applyDecorators(
        Expose(),
        Transform((target: { value: unknown }) => (target.value === undefined ? defaultValue : target.value)),
    );
};
