import { IsDate, isDefined, IsNotEmpty } from 'class-validator';
import { applyDecorators } from '@nestjs/common';
import { Transform } from 'class-transformer';
import { ValidationExceptions } from '../validations/exceptions/validation.exceptions';

export const IsDateStringTransform = () => {
    return applyDecorators(
        IsNotEmpty(),
        IsDate(),
        Transform((target) => {
            const date = target.value as string;

            // для случая, если target.value != null
            if (isDefined(date)) {
                IsExistingDateFunction(date);

                return new Date(date);
            }

            // если null, то валидация происходит в зависимости от @IsOptional()
            return date;
        }),
    );
};

export const IsExistingDateFunction = (value: string) => {
    const timeZoneIndex = value.indexOf('+');
    // чтобы можно было сравнить две даты, мы не должны учитывать time zone
    const dateWithoutTimeZone = timeZoneIndex === -1 ? value : value.slice(0, timeZoneIndex);

    try {
        // проверяем, равна ли дата из строки тому же объекту Date без учета time zone
        const validDate = new Date(dateWithoutTimeZone).toISOString().slice(0, 10) == value.slice(0, 10);

        if (!validDate) ValidationExceptions.invalidDate();
    } catch (e) {
        ValidationExceptions.invalidDate();
    }

    return true;
};
