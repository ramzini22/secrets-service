import { applyDecorators } from '@nestjs/common';
import { IsInt, Max, Min } from 'class-validator';
import { Transform } from 'class-transformer';
import { NumbersUtils } from '../../../utils/numbers.utils';

/**
 * validate is integer value
 */

export const IsInteger = (params: IsIntegerParams = {}): PropertyDecorator => {
    return applyDecorators(
        Transform((target) => {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            const value = target.value;

            if (typeof value === 'boolean' || value == 'true' || value == 'false') return 'boolean';

            // eslint-disable-next-line @typescript-eslint/no-unsafe-return
            return isNaN(Number(value)) ? value : value === null ? null : +value;
        }),
        IsInt(),
        Min(params.min ?? NumbersUtils.INTEGER_MIN),
        Max(params.max ?? NumbersUtils.INTEGER_MAX),
    );
};

export type IsIntegerParams = {
    max?: number;
    min?: number;
};
