import { applyDecorators, Injectable } from '@nestjs/common';
import { Validate, ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { StringUtils } from '../../../utils/string.utils';
import { ValidationExceptions } from './exceptions/validation.exceptions';

/**
 * validate is not empty string
 */
@ValidatorConstraint({ name: 'IsNotEmptyString' })
@Injectable()
export class IsNotEmptyStringRule implements ValidatorConstraintInterface {
    defaultMessage(validationArguments: ValidationArguments): string {
        return ValidationExceptions.emptyString(validationArguments.property);
    }

    validate(str: unknown): boolean {
        return StringUtils.isNotEmptyString(str);
    }
}

export const IsNotEmptyString = () => {
    return applyDecorators(Validate(IsNotEmptyStringRule, { message: 'should be is not empty string' }));
};
