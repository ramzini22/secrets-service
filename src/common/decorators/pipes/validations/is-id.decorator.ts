import { IsInteger } from './is-integer.decorator';

export const IsId = () => {
    return IsInteger({ min: 1 });
};
