import { applyDecorators } from '@nestjs/common';
import { IsNotEmpty, IsObject, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export const IsObjectOfType = (type: object) => {
    return applyDecorators(
        ValidateNested(),
        Type(() => type as never),
        IsNotEmpty(),
        IsObject(),
    );
};
