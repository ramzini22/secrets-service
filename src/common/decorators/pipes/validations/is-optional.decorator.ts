import { ValidateIf, ValidationOptions, IsOptional as IsOptionalValidator } from 'class-validator';

export function IsOptional(nullable = false, validationOptions?: ValidationOptions) {
    if (nullable) {
        return IsOptionalValidator(validationOptions);
    }

    return ValidateIf((ob: unknown, v: unknown) => {
        return v !== undefined;
    }, validationOptions);
}
