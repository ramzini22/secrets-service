import { createCustomBadRequestError } from '../../../../utils/error.utils';

export class ValidationExceptions {
    static emptyString(property: string) {
        return `Поле '${property}' должно быть не пустой строкой`;
    }

    static invalidDate() {
        return createCustomBadRequestError(`Нверный формат даты`);
    }
}
