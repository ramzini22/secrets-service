import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { CustomRequestType } from '../../types/custom-request.type';

export const Address = createParamDecorator((_data: unknown, ctx: ExecutionContext): string | undefined => {
    const request = ctx.switchToHttp().getRequest<CustomRequestType>();

    return request.address;
});
