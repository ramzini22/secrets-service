import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { CustomRequestType } from '../../types/custom-request.type';

export const UserAgent = createParamDecorator((_data: unknown, ctx: ExecutionContext): string => {
    const request = ctx.switchToHttp().getRequest<CustomRequestType>();

    return request.userAgent;
});
