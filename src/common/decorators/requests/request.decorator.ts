import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { CustomRequestType } from '../../types/custom-request.type';

export const Request = createParamDecorator((_data: unknown, ctx: ExecutionContext): CustomRequestType => {
    return ctx.switchToHttp().getRequest<CustomRequestType>();
});
