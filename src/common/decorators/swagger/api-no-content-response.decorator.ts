import { applyDecorators } from '@nestjs/common';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { Envs } from '../../envs/env';

export const ApiNoContentResponse = (description: string): MethodDecorator => {
    if (!Envs.swagger.isWriteConfig) return applyDecorators();

    return applyDecorators(
        ApiOperation({ description: `канал ${description}` }),
        ApiOkResponse({
            schema: {
                properties: {
                    success: { type: 'boolean' },
                    data: { type: 'object' },
                },
            },
        }),
    );
};
