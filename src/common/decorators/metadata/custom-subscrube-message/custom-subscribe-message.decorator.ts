import { applyDecorators, SetMetadata } from '@nestjs/common';
import { SubscribeMessage } from '@nestjs/websockets';
import { ApiTags } from '@nestjs/swagger';
import { Envs } from '../../../envs/env';
import { EVENT_NAME_METADATA_KEY } from './event-name.metadata-key';

export const CustomSubscribeMessage = (name: string, method: (value: string | string[]) => MethodDecorator) => {
    if (!Envs.swagger.isWriteConfig)
        return applyDecorators(SubscribeMessage(name), SetMetadata(EVENT_NAME_METADATA_KEY, name));

    return applyDecorators(
        SubscribeMessage(name),
        SetMetadata(EVENT_NAME_METADATA_KEY, name),
        ApiTags(name.split(':')[0]),
        method(name.replaceAll(':', '•')),
    );
};
