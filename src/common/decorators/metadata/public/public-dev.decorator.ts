import { applyDecorators, SetMetadata } from '@nestjs/common';
import { IS_PUBLIC_METADATA_KEY } from './cerification-type.metadata-key';

// must be not used
// decorator only for checking develop methods
export const PublicDev = () => applyDecorators(SetMetadata(IS_PUBLIC_METADATA_KEY, true));
