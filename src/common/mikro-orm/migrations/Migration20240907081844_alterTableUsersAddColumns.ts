import { Migration } from '@mikro-orm/migrations';

export class Migration20240907081844_alterTableUsersAddColumns extends Migration {
    up(): void {
        this.addSql(
            'alter table "users" add column "is_online" boolean not null default true, add column "last_online_at" timestamptz not null default now();',
        );
        this.addSql('alter table "users" add constraint "users_address_unique" unique ("address");');
        this.addSql('create index "users_address_index" on "users" ("address");');
    }

    down(): void {
        this.addSql('alter table "users" drop constraint "users_address_unique";');
        this.addSql('drop index "users_address_index";');
        this.addSql('alter table "users" drop column "is_online", drop column "last_online_at";');
    }
}
