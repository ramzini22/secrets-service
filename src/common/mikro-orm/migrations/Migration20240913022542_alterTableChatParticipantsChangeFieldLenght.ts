import { Migration } from '@mikro-orm/migrations';

export class Migration20240913022542_alterTableChatParticipantsChangeFieldLenght extends Migration {
    up(): void {
        this.addSql(
            'alter table "chat_participants" alter column "encrypt_aes_key" type varchar(4096) using ("encrypt_aes_key"::varchar(4096));',
        );
    }

    down(): void {
        this.addSql(
            'alter table "chat_participants" alter column "encrypt_aes_key" type varchar(1024) using ("encrypt_aes_key"::varchar(1024));',
        );
    }
}
