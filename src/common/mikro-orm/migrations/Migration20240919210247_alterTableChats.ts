import { Migration } from '@mikro-orm/migrations';

export class Migration20240919210247_alterTableChats extends Migration {
    up(): void {
        this.addSql("create type \"chat_statuses_enum\" as enum ('monolog', 'dialog', 'chat');");
        this.addSql('alter table "chats" drop column "is_dialog";');
        this.addSql('alter table "chats" add column "type" "chat_statuses_enum" not null;');
    }

    down(): void {
        this.addSql('alter table "chats" drop column "type";');
        this.addSql('alter table "chats" add column "is_dialog" bool not null;');
        this.addSql('drop type "chat_statuses_enum";');
    }
}
