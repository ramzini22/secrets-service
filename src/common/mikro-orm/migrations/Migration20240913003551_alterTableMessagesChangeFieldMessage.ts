import { Migration } from '@mikro-orm/migrations';

export class Migration20240913003551_alterTableMessagesChangeFieldMessage extends Migration {
    up(): void {
        this.addSql('alter table "messages" drop column "value";');
        this.addSql('alter table "messages" add column "encrypt_message" varchar(32768) not null;');
    }

    down(): void {
        this.addSql('alter table "messages" drop column "encrypt_message";');
        this.addSql('alter table "messages" add column "value" varchar(255) not null;');
    }
}
