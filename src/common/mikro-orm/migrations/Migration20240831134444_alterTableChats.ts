import { Migration } from '@mikro-orm/migrations';

export class Migration20240831134444_alterTableChats extends Migration {
    async up(): Promise<void> {
        this.addSql('alter table "chats" add column "creator_user_id" uuid not null;');
        this.addSql('alter table "chats" alter column "key" type varchar(255) using ("key"::varchar(255));');
        this.addSql('alter table "chats" alter column "key" drop not null;');
        this.addSql(
            'alter table "chats" add constraint "chats_creator_user_id_foreign" foreign key ("creator_user_id") references "users" ("id") on update cascade;',
        );

        this.addSql('alter table "chat_participants" drop column "chat_encrypt_key";');

        this.addSql('alter table "chat_participants" add column "encrypt_aes_key" varchar(1024) null;');
    }

    async down(): Promise<void> {
        this.addSql('alter table "chats" drop constraint "chats_creator_user_id_foreign";');

        this.addSql('alter table "chat_participants" drop column "encrypt_aes_key";');

        this.addSql('alter table "chat_participants" add column "chat_encrypt_key" varchar(255) null;');

        this.addSql('alter table "chats" drop column "creator_user_id";');

        this.addSql('alter table "chats" alter column "key" type varchar(255) using ("key"::varchar(255));');
        this.addSql('alter table "chats" alter column "key" set not null;');
    }
}
