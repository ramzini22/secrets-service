import { Migration } from '@mikro-orm/migrations';

export class Migration20240913024821_alterTableChatsAddColumnIsDialog extends Migration {
    up(): void {
        this.addSql('alter table "chats" add column "is_dialog" boolean not null;');
    }

    down(): void {
        this.addSql('alter table "chats" drop column "is_dialog";');
    }
}
