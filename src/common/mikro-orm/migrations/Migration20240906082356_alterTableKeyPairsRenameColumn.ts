import { Migration } from '@mikro-orm/migrations';

export class Migration20240906082356_alterTableKeyPairsRenameColumn extends Migration {
    up(): void {
        this.addSql('alter table "users_keys_pairs" rename column "private_key" to "encrypt_private_key";');
    }

    down(): void {
        this.addSql('alter table "users_keys_pairs" rename column "encrypt_private_key" to "private_key";');
    }
}
