import { Migration } from '@mikro-orm/migrations';

export class Migration20240705160419_init extends Migration {
    up(): void {
        this.addSql('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        this.addSql("create type \"user_message_enum\" as enum ('is_sent', 'is_gotten', 'is_read');");
        this.addSql(
            'create table "messages" ("id" uuid not null default uuid_generate_v4(), "created_at" timestamptz not null default now(), "updated_at" timestamptz not null default now(), "value" varchar(255) not null, constraint "messages_pkey" primary key ("id"));',
        );
        this.addSql('create index "messages_created_at_index" on "messages" ("created_at");');
        this.addSql(
            'create table "chats" ("id" uuid not null default uuid_generate_v4(), "created_at" timestamptz not null default now(), "updated_at" timestamptz not null default now(), "key" varchar(255) not null, "is_public" boolean not null default true, "avatar" varchar(255) null, "title" varchar(255) not null, "last_message_id" uuid null, constraint "chats_pkey" primary key ("id"));',
        );
        this.addSql('alter table "chats" add constraint "chats_last_message_id_unique" unique ("last_message_id");');
        this.addSql(
            'create table "users" ("id" uuid not null default uuid_generate_v4(), "created_at" timestamptz not null default now(), "updated_at" timestamptz not null default now(), "address" varchar(255) not null, "hash_passphrase" varchar(255) not null, constraint "users_pkey" primary key ("id"));',
        );
        this.addSql(
            'create table "chat_participants" ("id" uuid not null default uuid_generate_v4(), "created_at" timestamptz not null default now(), "updated_at" timestamptz not null default now(), "deleted_at" timestamptz null, "chat_encrypt_key" varchar(255) null, "chat_id" uuid not null, "user_id" uuid not null, constraint "chat_participants_pkey" primary key ("id"));',
        );
        this.addSql('create index "chat_participants_user_id_index" on "chat_participants" ("user_id");');
        this.addSql(
            'alter table "chat_participants" add constraint "chat_participants_chat_id_user_id_unique" unique ("chat_id", "user_id");',
        );
        this.addSql(
            'create table "users_keys_pairs" ("id" uuid not null default uuid_generate_v4(), "created_at" timestamptz not null default now(), "updated_at" timestamptz not null default now(), "public_key" varchar(1024) not null, "private_key" varchar(16384) not null, "user_id" uuid not null, constraint "users_keys_pairs_pkey" primary key ("id"));',
        );
        this.addSql(
            'alter table "users_keys_pairs" add constraint "users_keys_pairs_user_id_unique" unique ("user_id");',
        );
        this.addSql('create index "users_keys_pairs_user_id_index" on "users_keys_pairs" ("user_id");');
        this.addSql(
            'create table "user_messages" ("id" uuid not null default uuid_generate_v4(), "created_at" timestamptz not null default now(), "updated_at" timestamptz not null default now(), "status" "user_message_enum" not null default \'is_sent\', "chat_id" uuid not null, "message_id" uuid not null, "user_id" uuid not null, constraint "user_messages_pkey" primary key ("id"));',
        );
        this.addSql('create index "user_messages_created_at_index" on "user_messages" ("created_at");');
        this.addSql('create index "user_messages_status_index" on "user_messages" ("status");');
        this.addSql('create index "user_messages_chat_id_user_id_index" on "user_messages" ("chat_id", "user_id");');
        this.addSql(
            'alter table "user_messages" add constraint "user_messages_message_id_user_id_unique" unique ("message_id", "user_id");',
        );
        this.addSql(
            'alter table "chats" add constraint "chats_last_message_id_foreign" foreign key ("last_message_id") references "messages" ("id") on update cascade on delete set null;',
        );
        this.addSql(
            'alter table "chat_participants" add constraint "chat_participants_chat_id_foreign" foreign key ("chat_id") references "chats" ("id") on update cascade;',
        );
        this.addSql(
            'alter table "chat_participants" add constraint "chat_participants_user_id_foreign" foreign key ("user_id") references "users" ("id") on update cascade;',
        );
        this.addSql(
            'alter table "users_keys_pairs" add constraint "users_keys_pairs_user_id_foreign" foreign key ("user_id") references "users" ("id") on update cascade;',
        );
        this.addSql(
            'alter table "user_messages" add constraint "user_messages_chat_id_foreign" foreign key ("chat_id") references "chats" ("id") on update cascade;',
        );
        this.addSql(
            'alter table "user_messages" add constraint "user_messages_message_id_foreign" foreign key ("message_id") references "messages" ("id") on update cascade;',
        );
        this.addSql(
            'alter table "user_messages" add constraint "user_messages_user_id_foreign" foreign key ("user_id") references "users" ("id") on update cascade;',
        );
    }

    down(): void {
        this.addSql('alter table "chats" drop constraint "chats_last_message_id_foreign";');
        this.addSql('alter table "user_messages" drop constraint "user_messages_message_id_foreign";');
        this.addSql('alter table "chat_participants" drop constraint "chat_participants_chat_id_foreign";');
        this.addSql('alter table "user_messages" drop constraint "user_messages_chat_id_foreign";');
        this.addSql('alter table "chat_participants" drop constraint "chat_participants_user_id_foreign";');
        this.addSql('alter table "users_keys_pairs" drop constraint "users_keys_pairs_user_id_foreign";');
        this.addSql('alter table "user_messages" drop constraint "user_messages_user_id_foreign";');
        this.addSql('drop table if exists "messages" cascade;');
        this.addSql('drop table if exists "chats" cascade;');
        this.addSql('drop table if exists "users" cascade;');
        this.addSql('drop table if exists "chat_participants" cascade;');
        this.addSql('drop table if exists "users_keys_pairs" cascade;');
        this.addSql('drop table if exists "user_messages" cascade;');
        this.addSql('drop type "user_message_enum";');
        this.addSql('drop type "user_message_enum";');
    }
}
