import { Migration } from '@mikro-orm/migrations';

export class Migration20240907125959_alterTableChatChangeColumnTitleNullable extends Migration {
    up(): void {
        this.addSql('alter table "chats" alter column "title" type varchar(255) using ("title"::varchar(255));');
        this.addSql('alter table "chats" alter column "title" drop not null;');
    }

    down(): void {
        this.addSql('alter table "chats" alter column "title" type varchar(255) using ("title"::varchar(255));');
        this.addSql('alter table "chats" alter column "title" set not null;');
    }
}
