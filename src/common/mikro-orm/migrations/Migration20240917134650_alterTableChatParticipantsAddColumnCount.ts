import { Migration } from '@mikro-orm/migrations';

export class Migration20240917134650_alterTableChatParticipantsAddColumnCount extends Migration {
    up(): void {
        this.addSql('alter table "chat_participants" add column "count_is_send_messages" int not null default 0;');
    }

    down(): void {
        this.addSql('alter table "chat_participants" drop column "count_is_send_messages";');
    }
}
