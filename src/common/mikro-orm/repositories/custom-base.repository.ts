import { EntityData, FilterQuery, QueryBuilder, SqlEntityRepository } from '@mikro-orm/postgresql';

export class CustomBaseRepository<ENTITY extends object> extends SqlEntityRepository<ENTITY> {
    protected ALIAS!: string;

    public getSelectQueryBuilder(): QueryBuilder<ENTITY> {
        return this.createQueryBuilder(this.ALIAS);
    }

    public async updateOne(where: FilterQuery<ENTITY>, data: EntityData<ENTITY>): Promise<ENTITY | null> {
        const updateObject = Object.fromEntries(
            Object.entries(data).filter(([key, value]) => value !== undefined && key),
        );
        const result = await this.nativeUpdate(where, <EntityData<ENTITY>>updateObject);

        if (!result) return null;

        return this.findOne(where);
    }
}
