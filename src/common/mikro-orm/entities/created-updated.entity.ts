import { Property } from '@mikro-orm/core';
import { CreatedEntity } from './created.entity';

export class CreatedUpdatedEntity extends CreatedEntity {
    @Property({ defaultRaw: 'NOW()' })
    readonly updatedAt!: Date;
}
