import { PrimaryKey } from '@mikro-orm/core';

export class BaseEntity {
    @PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
    id!: string;
}
