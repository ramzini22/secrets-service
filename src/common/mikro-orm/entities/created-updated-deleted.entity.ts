import { Property } from '@mikro-orm/core';
import { CreatedUpdatedEntity } from './created-updated.entity';

export class CreatedUpdatedDeletedEntity extends CreatedUpdatedEntity {
    @Property({ nullable: true })
    readonly deletedAt!: Date;
}
