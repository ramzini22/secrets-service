import * as process from 'process';
import { config } from 'dotenv';
import { NumbersUtils } from '../utils/numbers.utils';
import { BooleanUtils } from '../utils/boolean.utils';

config();

export const Envs = {
    main: {
        host: process.env.TC_APP_HOST || '0.0.0.0',
        appPort: NumbersUtils.toNumberOrDefault(process.env.TC_APP_PORT, 3000),
        socketIoPort: NumbersUtils.toNumberOrDefault(process.env.TC_SOCKET_IO_PORT, 3001),
        sharedSecret: process.env.TC_SHARED_SECRET || '',
        domain: process.env.TC_DOMAIN || '',
        payloadTtl: 3600, // 1 hour
        proofTtl: 3600, // 1 hour
    },

    postgres: {
        host: process.env.TC_PG_HOST,
        port: NumbersUtils.toNumberOrDefault(process.env.TC_PG_PORT, 5432),
        name: process.env.TC_PG_DATABASE,
        username: process.env.TC_PG_USERNAME,
        password: process.env.TC_PG_PASSWORD,
        migrationsRun: BooleanUtils.strToBoolWithDefault(process.env.TC_PG_MIGRATIONS_RUN, false),
        logging: BooleanUtils.strToBoolWithDefault(process.env.TC_PG_LOGGINING, false),
    },

    swagger: {
        path: process.env.TC_SWAGGER_PATH || 'docs',
        isWriteConfig: BooleanUtils.strToBoolWithDefault(process.env.TC_SWAGGER_IS_WRITE_CONFIG, false),
        url: `http://localhost:${process.env.TC_APP_PORT ?? 3000}`,
        description: 'development',
    },

    redis: {
        host: process.env.TC_REDIS_HOST || '',
        port: NumbersUtils.toNumberOrDefault(process.env.TC_REDIS_PORT, 6379),
        db: NumbersUtils.toNumberOrDefault(process.env.TC_REDIS_DB, 0),
    },
};
