import { WsException } from '@nestjs/websockets';
import { ValidationError } from '@nestjs/common';

export class CustomError extends WsException {
    type: errorEnum;
    details: ValidationError[] = [];

    constructor(error: string | object, type: errorEnum) {
        super(error);
        this.type = type;

        if (typeof error === 'object') this.details = <ValidationError[]>error;
    }
}

export enum errorEnum {
    BAD_REQUEST = 'Плохой запрос.',
    SERVER_ERROR = 'Ошибка серверва.',
    FORBIEN = 'Ошибка доступа.',
}
