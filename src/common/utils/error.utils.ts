import { CustomError, errorEnum } from '../errors/custom-error';

const createCustomWsError = (error: string | object, type: errorEnum): CustomError => {
    return new CustomError(error, type);
};

export const createCustomBadRequestError = (error: string | object) =>
    createCustomWsError(error, errorEnum.BAD_REQUEST);

export const createCustomForbienError = () => createCustomWsError('Нет доступа к ресуру', errorEnum.FORBIEN);

export const createCustomServerError = () => createCustomWsError('Неизвестаня ошибка сервера', errorEnum.SERVER_ERROR);
