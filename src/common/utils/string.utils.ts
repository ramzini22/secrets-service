export class StringUtils {
    public static isNotEmptyString(str?: unknown): boolean {
        return Boolean(str && typeof str === 'string' && str.trim());
    }
}
