export class DateUtils {
    public static readonly MIN_IN_MS = 60000;
    public static readonly HOUR_IN_MS = DateUtils.MIN_IN_MS * 60;

    static withoutMinute(day: Date): Date {
        return new Date(day.toISOString().split(':')[0] + ':00:00.000Z');
    }

    public static now(): Date {
        return new Date();
    }

    static addDays(oldDate: Date, days: number): Date {
        return DateUtils.addHours(oldDate, days * 24);
    }

    static addHours(oldDate: Date, hours: number): Date {
        const newDate = new Date(oldDate);
        newDate.setTime(newDate.getTime() + hours * DateUtils.HOUR_IN_MS);

        return newDate;
    }

    public static tomorrow(): Date {
        return DateUtils.addDays(DateUtils.now(), 1);
    }
}
