import { IsUUID } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional } from '../../decorators/pipes/validations/is-optional.decorator';
import { QueryLimitOffsetDto } from './query-limit-offset.dto';

export class QueryIdLimitOffsetDto extends QueryLimitOffsetDto {
    @ApiPropertyOptional({ description: 'id' })
    @IsOptional()
    @IsUUID()
    readonly id?: string;
}
