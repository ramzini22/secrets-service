import { ApiPropertyOptional } from '@nestjs/swagger';
import { Max } from 'class-validator';
import { IsOptional } from '../../decorators/pipes/validations/is-optional.decorator';
import { Default } from '../../decorators/pipes/transform/default-value.decorator';
import { IsInteger } from '../../decorators/pipes/validations/is-integer.decorator';
import { IsId } from '../../decorators/pipes/validations/is-id.decorator';

export class QueryLimitOffsetDto {
    @ApiPropertyOptional({ description: 'кол-во записей', maximum: 100, minimum: 1, default: 100 })
    @IsId()
    @IsOptional()
    @Max(100)
    @Default(100)
    limit?: number;

    @ApiPropertyOptional({ description: 'кол-во записей, которые нужно пропустить', minimum: 1 })
    @IsInteger()
    @IsOptional()
    offset?: number;
}
