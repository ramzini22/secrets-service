import { errorEnum } from '../../errors/custom-error';

export class DataError {
    readonly success: boolean;
    readonly data: dataType;
    constructor(type: errorEnum, details: (string | validationError)[], eventName: string) {
        this.success = false;
        this.data = {
            type,
            details,
            eventName,
        };
    }
}

export type validationError = {
    column: string;
    error: string;
};

type dataType = {
    type: errorEnum;
    details: (string | validationError)[];
    eventName: string;
};
