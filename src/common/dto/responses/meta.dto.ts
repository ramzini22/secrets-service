import { ApiProperty } from '@nestjs/swagger';

export class MetaDto {
    @ApiProperty({ description: 'общее количество записей' })
    readonly count: number;

    constructor(count: number) {
        this.count = count;
    }
}
