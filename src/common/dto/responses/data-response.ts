export class DataResponse<T extends object> {
    protected data!: T | undefined;
    public success: boolean;

    constructor(data?: T) {
        this.success = !!data;

        if (data) this.data = data;
    }

    setData(data?: T): DataResponse<T> {
        this.success = !!data;
        this.data = data;

        return this;
    }

    setSuccess(success: boolean): DataResponse<T> {
        this.success = success;

        return this;
    }

    getData(): T | undefined {
        return this.data;
    }
}
