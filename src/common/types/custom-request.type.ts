import http from 'http';
import { Handshake } from 'socket.io/dist/socket';
import { ExecutionContext } from '@nestjs/common';

export interface CustomRequestType extends http.IncomingMessage {
    handshake: Handshake;
    userAgent: string;
    eventName: string;
    userId?: string;
    address?: string;
    context: ExecutionContext;
}
